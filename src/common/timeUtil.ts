import dayjs from 'dayjs';

export const formatTimeStr = (dateStr: string) => {
  const date = dayjs(dateStr);
  if (!date.isValid()) {
    return `date is not valid (${dateStr})`;
  }
  return date.format('YYYY/MM/DD');
};
