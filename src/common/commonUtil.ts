import { delay } from 'lodash';

export function selectRandomNum<T>(arr: T[], count: number): T[] {
  if (arr.length <= count) {
    return arr;
  }
  const result: T[] = [];
  const copyArr = [...arr];
  for (let i = 0; i < count; i++) {
    const randomIndex = Math.floor(Math.random() * copyArr.length);
    result.push(copyArr[randomIndex]);
    copyArr.splice(randomIndex, 1);
  }
  return result;
}

export function delaySafe<T extends (...args: any[]) => any>(
  func: T,
  wait: number,
  ...args: Parameters<T>
): number {
  return delay(func, wait, ...args);
}
