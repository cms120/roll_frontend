import { IDrawRound, TExpert } from '../../api/handle';
import _, { cloneDeep } from 'lodash';

/**
 * 获取当前轮次抽签的选出来的专家
 * @param drawRound
 */
export const getExpertsDrewFromDrawRound = (
  drawRound: Pick<IDrawRound, 'current_experts' | 'status_list'>,
): TExpert['index'][] => {
  return drawRound.current_experts.filter(
    (expert, index) =>
      drawRound.status_list[index] === 3 || drawRound.status_list[index] === 2,
  );
};
/**
 * 获取当前轮次抽选出来的专家的状态
 */
export const getExpertsStatusDrewFromDrawRound = (
  drawRound: Pick<IDrawRound, 'status_list'>,
): IDrawRound['status_list'] => {
  return drawRound.status_list.filter((status) => status === 2 || status === 3);
};

/**
 * 筛选出当前轮次中可以被抽签的专家
 */
export const getExpertsQualifiedToBeDrew = (
  drawRound: Pick<IDrawRound, 'current_experts' | 'status_list'>,
): TExpert['index'][] => {
  return drawRound.current_experts.filter(
    (expert, index) =>
      drawRound.status_list[index] !== 1 && drawRound.status_list[index] !== 4,
  );
};

/**
 * 从当前的抽签结果中随机选取一定数量的专家
 * @param current_experts
 * @param status_list 选取的专家不能是 status_list 为 1 的, 1 代表被替换下去的,
 * @param randomNumAll 要抽选的专家数量 包含已经抽选的
 * @param ifReplaceExist 是否替换已经选中的专家，不替换的话即为增选
 * 替换的话，即为重新抽取
 */
export const selectRandomExperts = (
  current_experts: TExpert['index'][],
  randomNumAll = 1,
  status_list?: IDrawRound['status_list'],
  ifReplaceExist = true,
): TExpert['index'][] => {
  // 去除被 手动替换下去的专家 , 获得有资格参加抽选的
  const expertsQualified =
    status_list == null
      ? current_experts
      : getExpertsQualifiedToBeDrew({
          current_experts,
          status_list,
        });
  let numToBeSelect = randomNumAll;

  if (!ifReplaceExist) {
    // 增选 即对已经选中的专家不作修改
    if (status_list == null) {
      console.error(
        '如果要增选，需要给出状态码',
        current_experts,
        randomNumAll,
      );
    } else {
      // 已经选中的专家
      const expertsSelected = getExpertsDrewFromDrawRound({
        current_experts,
        status_list,
      });
      numToBeSelect -= expertsSelected.length; // 剩余需要抽取的专家数量
      // 候选的专家 将已经选中的专家从候选中去除
      const expertsCandidate = _.difference(expertsQualified, expertsSelected);
      return _.union(
        expertsSelected,
        _.sampleSize(expertsCandidate, numToBeSelect),
      );
    }
  }
  return _.sampleSize(expertsQualified, randomNumAll);
};

/**
 * 创建一个新的抽签轮次
 * @param expertsAll 所有专家
 * @param expertsDrew 已经抽签的专家
 */
export const createDrawRoundByExperts = (
  expertsAll: TExpert['index'][],
  expertsDrew: TExpert['index'][],
): IDrawRound => {
  return {
    current_experts: expertsAll,
    status_list: expertsAll.map((expert) =>
      expertsDrew.includes(expert) ? 3 : 0,
    ),
    if_log: 0,
  };
};
/**
 *
 * @param drawRound 当前轮选举
 * @param experts 新的需要保留的experts
 * @constructor
 */
export const updateDrawRoundExperts = (
  drawRound: IDrawRound,
  experts: TExpert['index'][],
): IDrawRound => {
  const res = cloneDeep(drawRound);
  // todo
  for (let i = 0; i < res.current_experts.length; i++) {
    switch (res.status_list[i]) {
      case 0: // 没有抽中
        if (experts.includes(res.current_experts[i])) {
          // 中了
          res.status_list[i] = 2; //没抽中，但是被替换上来了
        }
        break;
      case 1: // 抽中了，但是被别人替换了, 替换之后不能被抽中
        if (experts.includes(res.current_experts[i])) {
          console.error(
            '抽中了，但是被别人替换了，不能再被抽中',
            drawRound,
            experts,
          );
        }
        break;
      case 2: // 被替换上来的
        if (!experts.includes(res.current_experts[i])) {
          res.status_list[i] = 1; // 中了，但被替换下去
        }
        break;
      case 3: // 正常抽中，且没被替换
        if (!experts.includes(res.current_experts[i])) {
          res.status_list[i] = 1; // 中了，但被替换下去
        }
        break;
      case 4:
        console.warn('最后来的，不能被替换', drawRound, experts);
        break;
    }
  }
  return res;
};

/**
 *
 * @param drawRound 当前轮选举
 * @param expertsToRemove 需要去除的 experts
 * @constructor
 */

export const removeDrawRoundExperts = (
  drawRound: IDrawRound,
  expertsToRemove: TExpert['index'][],
): IDrawRound => {
  // 当前的抽签得到的专家
  const expertsDrewPrev = getExpertsDrewFromDrawRound(drawRound);
  const expertsNew = _.difference(expertsDrewPrev, expertsToRemove);
  return updateDrawRoundExperts(drawRound, expertsNew);
};

/**
 * @param drawRound 当前轮选举
 * @param expertsToAdd 需要添加的 experts
 * @constructor
 */
export const importDrawRoundExperts = (
  drawRound: IDrawRound,
  expertsToAdd: TExpert['index'][],
): IDrawRound => {
  // 当前的抽签得到的专家
  const expertsDrewPrev = getExpertsDrewFromDrawRound(drawRound);
  const expertsNew = _.union(expertsDrewPrev, expertsToAdd);
  return updateDrawRoundExperts(drawRound, expertsNew);
};
