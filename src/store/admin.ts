import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export const Admin_Password = '18955981';
export interface TAdminState {
  adminCheckOpen: boolean;

  adminCheckCB?: (check: boolean) => void;
}
const initialState: TAdminState = {
  adminCheckOpen: false,
};

export const adminSlice = createSlice({
  name: 'admin',
  initialState,
  reducers: {
    setAdminCheckOpen(state, action: PayloadAction<boolean>) {
      state.adminCheckOpen = action.payload;
    },
    setCheckCB(state, action: PayloadAction<TAdminState['adminCheckCB']>) {
      state.adminCheckCB = action.payload;
    },
  },
});

export const { setAdminCheckOpen, setCheckCB } = adminSlice.actions;

export const selectAdminCheckOpen = (state: { admin: TAdminState }) =>
  state.admin.adminCheckOpen;

export const selectAdminCheckCB = (state: { admin: TAdminState }) =>
  state.admin.adminCheckCB;
