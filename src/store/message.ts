import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ArgsProps } from 'antd/es/message/interface';

export interface TMessageState {
  argsProps: ArgsProps | null;
}

const initialState: TMessageState = {
  argsProps: null,
};

export const messageSlice = createSlice({
  name: 'message',
  initialState,
  reducers: {
    setMessage(state, action: PayloadAction<ArgsProps>) {
      state.argsProps = action.payload;
    },
  },
});

export const { setMessage } = messageSlice.actions;

export const selectArgsProps = (state: { message: TMessageState }) =>
  state.message.argsProps;
