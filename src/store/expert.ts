import { TExpert, TFetchStatus } from '../api/handle';
import {
  createAsyncThunk,
  createSelector,
  createSlice,
  PayloadAction,
} from '@reduxjs/toolkit';
import { apiGetExpertList } from '../api/expert';
import { message } from 'antd';

export interface TExpertState {
  experts: TExpert[] | null;
  expertsFetchStatus: TFetchStatus;
  expertsFetchError: string | null;
}

const initialState: TExpertState = {
  experts: null,
  expertsFetchStatus: 'idle',
  expertsFetchError: null,
};

export const thunkFetchExperts = createAsyncThunk(
  'experts/fetchExperts',
  async () => {
    const response = await apiGetExpertList();
    return response.data;
  },
);

export const expertSlice = createSlice({
  name: 'expert',
  initialState,
  reducers: {
    setExpertsFetchStatus(state, action: PayloadAction<TFetchStatus>) {
      state.expertsFetchStatus = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(thunkFetchExperts.pending, (state) => {
        state.expertsFetchStatus = 'loading';
      })
      .addCase(thunkFetchExperts.fulfilled, (state, action) => {
        state.expertsFetchStatus = 'success';
        state.experts = action.payload;
      })
      .addCase(thunkFetchExperts.rejected, (state, action) => {
        state.expertsFetchStatus = 'error';
        state.expertsFetchError =
          action.error.message || JSON.stringify(action.error);
        message.error(`获取专家列表失败\t${state.expertsFetchError}`);
      });
  },
});

export const { setExpertsFetchStatus } = expertSlice.actions;

export const selectExperts = (state: { expert: TExpertState }) =>
  state.expert.experts;

export const selectExpert = (
  state: { expert: TExpertState },
  idx: TExpert['index'],
) => state.expert.experts?.find((expert) => expert.index === idx);

export const selectExpertsIdx = createSelector(selectExperts, (experts) =>
  experts?.map((expert) => expert.index),
);

const selectExpertsIdxInput = (
  _: { expert: TExpertState },
  idxArr: TExpert['index'][],
) => idxArr;
export const selectExpertsSome = createSelector(
  selectExperts,
  selectExpertsIdxInput,
  (experts, idxArr) =>
    experts?.filter((expert) => idxArr.includes(expert.index)),
);

export const selectExpertsFetchStatus = (state: { expert: TExpertState }) =>
  state.expert.expertsFetchStatus;
export const selectExpertsFetchError = (state: { expert: TExpertState }) =>
  state.expert.expertsFetchError;
