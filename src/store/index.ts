import { configureStore } from '@reduxjs/toolkit';
import { messageSlice } from './message';
import { expertSlice } from './expert';

import { drawingSlice } from './drawing';
import { adminSlice } from './admin';

export const store = configureStore({
  reducer: {
    message: messageSlice.reducer,
    expert: expertSlice.reducer,
    drawing: drawingSlice.reducer,
    admin: adminSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
