import { createSelector } from '@reduxjs/toolkit';
import { selectDrawLog } from './DrawLog';
import { getExpertsDrewFromDrawRound } from '../../common/Draw/DrawLog';

export const selectDrawCntSecond = createSelector(
  selectDrawLog,
  (drawLog) => drawLog?.second_cnt,
);
export const selectDrawRoundSecond = createSelector(
  selectDrawLog,
  (drawLog) => {
    if (drawLog == null || drawLog.rounds.length < 2) {
      return null;
    }
    return drawLog.rounds[1];
  },
);

export const selectIfLockRoundSecond = createSelector(
  selectDrawRoundSecond,
  (drawRound) => {
    if (drawRound == null) {
      return 0;
    }
    return drawRound.if_log;
  },
);

/**
 * 第二轮所有的专家
 *
 */
export const selectExpertsRoundSecond = createSelector(
  selectDrawRoundSecond,
  (drawRound) => {
    if (drawRound == null) {
      return [];
    }
    return drawRound.current_experts;
  },
);

export const selectExpertsStatusDrewRoundSecond = createSelector(
  selectDrawRoundSecond,
  (drawRoundSecond) => {
    if (drawRoundSecond == null) {
      return [];
    }
    return drawRoundSecond.status_list.filter(
      (status) => status === 2 || status === 3,
    );
  },
);
/**
 * 第二轮挑选的专家
 * @param state
 */
export const selectExpertsDrewRoundSecond = createSelector(
  selectDrawRoundSecond,
  (drawRoundSecond) => {
    if (drawRoundSecond == null) {
      return [];
    }
    return getExpertsDrewFromDrawRound(drawRoundSecond);
  },
);
