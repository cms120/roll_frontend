import { TDrawLog, TDrawLogInList, TFetchStatus } from '../../api/handle';
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { apiGetSelectLog, apiGetSelectLogList } from '../../api/draw';

export interface TDrawingState {
  drawLogList: TDrawLogInList[];
  drawLogListFetchStatus: TFetchStatus;
  drawLogListFetchError: string | null;

  drawLog: TDrawLog | null;
  drawLogFetchStatus: TFetchStatus;
  drawLogFetchError: string | null;
}

const initialState: TDrawingState = {
  drawLogList: [],
  drawLogListFetchStatus: 'idle',
  drawLogListFetchError: null,

  drawLog: null,
  drawLogFetchStatus: 'idle',
  drawLogFetchError: null,
};
export const thunkFetchDrawLogs = createAsyncThunk(
  'drawing/fetchDrawLogs',
  async () => {
    const response = await apiGetSelectLogList();
    return response.data;
  },
);
export const thunkFetchDrawLog = createAsyncThunk(
  'drawing/fetchDrawLog',
  async (index: number) => {
    const response = await apiGetSelectLog(index);
    return response.data;
  },
);
export const drawingSlice = createSlice({
  name: 'drawing',
  initialState,
  reducers: {
    setDrawLog(state, action: PayloadAction<TDrawLog | null>) {
      state.drawLog = action.payload;
    },
    setDrawLogListFetchStatus(state, action: PayloadAction<TFetchStatus>) {
      state.drawLogListFetchStatus = action.payload;
    },
    setDrawLogFetchStatus(state, action: PayloadAction<TFetchStatus>) {
      state.drawLogFetchStatus = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(thunkFetchDrawLogs.pending, (state) => {
        state.drawLogListFetchStatus = 'loading';
      })
      .addCase(thunkFetchDrawLogs.fulfilled, (state, action) => {
        state.drawLogListFetchStatus = 'success';
        state.drawLogList = action.payload;
      })
      .addCase(thunkFetchDrawLogs.rejected, (state, action) => {
        state.drawLogListFetchStatus = 'error';
        state.drawLogListFetchError = action.error.message || null;
      });
    builder
      .addCase(thunkFetchDrawLog.pending, (state) => {
        state.drawLogFetchStatus = 'loading';
      })
      .addCase(thunkFetchDrawLog.fulfilled, (state, action) => {
        state.drawLogFetchStatus = 'success';
        state.drawLog = action.payload;
      })
      .addCase(thunkFetchDrawLog.rejected, (state, action) => {
        state.drawLogFetchStatus = 'error';
        state.drawLogFetchError = action.error.message || null;
      });
  },
});
export const selectDrawLogFetchStatus = (state: { drawing: TDrawingState }) =>
  state.drawing.drawLogFetchStatus;
export const selectDrawLogFetchError = (state: { drawing: TDrawingState }) =>
  state.drawing.drawLogFetchError;

export const { setDrawLogFetchStatus, setDrawLogListFetchStatus, setDrawLog } =
  drawingSlice.actions;
