import { TDrawingState } from './drawing';

export const selectDrawLogs = (state: { drawing: TDrawingState }) =>
  state.drawing.drawLogList;
export const selectDrawLogsFetchStatus = (state: { drawing: TDrawingState }) =>
  state.drawing.drawLogListFetchStatus;
export const selectDrawLogsFetchError = (state: { drawing: TDrawingState }) =>
  state.drawing.drawLogListFetchError;

export const selectDrawLogInList = (
  state: { drawing: TDrawingState },
  index: number,
) => state.drawing.drawLogList.find((log) => log.index === index);
export const selectDrawLog = (state: { drawing: TDrawingState }) =>
  state.drawing.drawLog;
