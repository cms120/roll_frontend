import { createSelector } from '@reduxjs/toolkit';
import { selectDrawLog } from './DrawLog';
import { getExpertsDrewFromDrawRound } from '../../common/Draw/DrawLog';

export const selectDrawCntFirst = createSelector(
  selectDrawLog,
  (drawLog) => drawLog?.first_cnt,
);
export const selectDrawRoundFirst = createSelector(selectDrawLog, (drawLog) => {
  if (drawLog == null || drawLog.rounds.length < 1) {
    return null;
  }
  return drawLog.rounds[0];
});

export const selectIfLockRoundFirst = createSelector(
  selectDrawRoundFirst,
  (drawRoundFirst) => {
    if (drawRoundFirst == null) {
      return 0;
    }
    return drawRoundFirst.if_log;
  },
);

/**
 * 第一轮所有的专家
 *
 */
export const selectExpertsRoundFirst = createSelector(
  selectDrawRoundFirst,
  (drawRoundFirst) => {
    if (drawRoundFirst == null) {
      return [];
    }
    return drawRoundFirst.current_experts;
  },
);
/**
 * 第一轮挑选的专家
 * @param state
 */
export const selectExpertsDrewRoundFirst = createSelector(
  selectDrawRoundFirst,
  (drawRoundFirst) => {
    if (drawRoundFirst == null) {
      return [];
    }
    return getExpertsDrewFromDrawRound(drawRoundFirst);
  },
);
/**
 * 第一轮挑选的专家的状态列表
 *
 */
export const selectExpertsStatusDrewRoundFirst = createSelector(
  selectDrawRoundFirst,
  (drawRoundFirst) => {
    if (drawRoundFirst == null) {
      return [];
    }
    return drawRoundFirst.status_list.filter(
      (status) => status === 2 || status === 3,
    );
  },
);
