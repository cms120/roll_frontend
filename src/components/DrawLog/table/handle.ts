import { TDrawLogInList } from '../../../api/handle';
import React from 'react';

export interface IDrawLogTableProps {
  data: TDrawLogInList[];
  onEditRow: (key: React.Key) => void;
  onContinueRow: (key: React.Key) => void;
  onRefresh: () => void;
  // round index 只是在数组中的 index 不是自身的数据
  onRoundTagClick?: (round_index: number) => void;
  loading?: boolean;
}

export interface TDrawLogTableField extends TDrawLogInList {
  key: React.Key;
}
