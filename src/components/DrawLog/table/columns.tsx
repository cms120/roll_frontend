import { Button, Popconfirm, TableProps, Tag } from 'antd';
import { formatTimeStr } from '../../../common/timeUtil';
import cn from 'classnames';
import React from 'react';
import { TDrawLogTableField } from './handle';

export const getTableColumns = (
  onEditRow: (key: React.Key) => void,
  onContinueRow: (key: React.Key) => void,
  onDelRow: (key: React.Key) => void,
  // round index 只是在数组中的 index 不是自身的数据
  onRoundTagClick?: (round_index: number) => void,
): TableProps<TDrawLogTableField>['columns'] => {
  return [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
      showSorterTooltip: { target: 'full-header' },
      sorter: (a, b) => a.index - b.index,
      sortDirections: ['ascend', 'descend'],
    },
    {
      title: '标题',
      dataIndex: 'title',
      key: 'title',
      showSorterTooltip: { target: 'full-header' },
      sorter: (a, b) => a.title.localeCompare(b.title),
      sortDirections: ['descend', 'ascend'],
    },
    {
      title: '时间',
      dataIndex: 'time',
      key: 'time',
      showSorterTooltip: { target: 'full-header' },
      sorter: (a, b) => a.time.localeCompare(b.time),
      sortDirections: ['descend', 'ascend'],
      render: formatTimeStr,
    },
    {
      title: '筛选轮数',
      dataIndex: 'round_count',
      key: 'round_count',
      showSorterTooltip: { target: 'full-header' },
      sorter: (a, b) => a.round_count - b.round_count,
      sortDirections: ['descend', 'ascend'],
      render: (val, record) => {
        // 生成 round_count 个 tag
        const round_idx_list = Array.from(
          { length: record.round_count },
          (_, i) => i + 1,
        );
        return (
          <div className={cn('hstack', 'gap-1', 'flex-wrap')}>
            {round_idx_list.map((idx) => (
              <Tag
                key={idx}
                color={`hsl(240, 100%, ${100 - idx * 10}%)`}
                onClick={() => onRoundTagClick && onRoundTagClick(idx)}
              >
                {`第${idx}轮`}
              </Tag>
            ))}
          </div>
        );
      },
    },
    {
      title: '操作',
      key: 'operation',
      render: (_, record) => (
        <div className={cn('hstack', 'gap-1')}>
          <Button type="primary" onClick={() => onEditRow(record.key)}>
            编辑
          </Button>
          <Button type="primary" onClick={() => onContinueRow(record.key)}>
            继续抽签
          </Button>
          <Popconfirm
            title="确认删除?"
            onConfirm={() => onDelRow(record.key)}
            okText="确认"
            cancelText="取消"
          >
            <Button type="primary" danger>
              删除
            </Button>
          </Popconfirm>
        </div>
      ),
    },
  ];
};
