import React, { FC, useMemo, useState } from 'react';
import { apiDelSelectLog } from '../../../api/draw';
import { Button, message, Popconfirm, Table, TableProps } from 'antd';
import cn from 'classnames';
import type { TableRowSelection } from 'antd/es/table/interface';
import styles from '../../expert/table/index.module.scss';
import { IDrawLogTableProps, TDrawLogTableField } from './handle';
import { getTableColumns } from './columns';

export const DrawLogTable: FC<IDrawLogTableProps> = ({
  data,
  onEditRow,
  onContinueRow,
  onRefresh,
  onRoundTagClick,
  loading,
}) => {
  const handleDel = async (key: React.Key | React.Key[]) => {
    try {
      let keys: React.Key[] = [];
      if (Array.isArray(key)) {
        keys = key;
      } else {
        keys.push(key);
      }
      const res = await Promise.all(
        keys.map((k) => apiDelSelectLog(Number(k))),
      );
      if (res.length === 1) {
        message.success('删除成功');
      } else if (res.length > 1) {
        message.success('批量删除成功');
      }
    } catch (e) {
      console.error(e);
      message.error('删除失败');
    } finally {
      onRefresh();
    }
  };
  const columns: TableProps<TDrawLogTableField>['columns'] = useMemo(
    () => getTableColumns(onEditRow, onContinueRow, handleDel, onRoundTagClick),
    [data],
  );

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const onSelectChange: TableRowSelection<TDrawLogTableField>['onChange'] = (
    newSelectedRowKeys: React.Key[],
  ) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const dataSource = useMemo(
    () => data.map((expert) => ({ ...expert, key: expert.index })),
    [data],
  );
  const hasSelected = selectedRowKeys.length > 0;

  return (
    <div className={cn('vstack', 'gap-2')}>
      <div className={cn('hstack', 'gap-1')}>
        <Popconfirm
          title="确认删除?"
          onConfirm={() => handleDel(selectedRowKeys)}
          okText="确认"
          cancelText="取消"
        >
          <Button type="primary" danger disabled={!hasSelected}>
            删除
          </Button>
        </Popconfirm>
      </div>
      <Table
        className={styles['draw-log-table-root']}
        columns={columns}
        dataSource={dataSource}
        loading={loading}
        bordered
        showHeader={true}
        rowSelection={{
          type: 'checkbox',
          selectedRowKeys,
          selections: true,
          onChange: onSelectChange,
        }}
      />
    </div>
  );
};
