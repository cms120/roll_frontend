import { TDrawLog } from '../../../api/handle';
import React, { FC } from 'react';
import cn from 'classnames';
import { formatTimeStr } from '../../../common/timeUtil';

export interface IDrawLogItemProps {
  drawLog: TDrawLog;
}

export const DrawLogItem: FC<IDrawLogItemProps> = ({ drawLog }) => {
  return (
    <div className={cn('vstack', 'gap-1')}>
      <span>题目：{drawLog.title}</span>
      <span>开始时间：{formatTimeStr(drawLog.time)}</span>
      <span>已选举轮数：{drawLog.rounds.length}</span>
    </div>
  );
};
