import React, { FC, useEffect, useMemo, useState } from 'react';
import { Button, FormInstance, message, Modal, ModalProps } from 'antd';
import { DrawLogForm } from './form';
import { TDrawLog } from '../../../api/handle';
import { apiGetSelectLog, apiUpdateSelectLog } from '../../../api/draw';
import {
  convertDrawLogToFormField,
  convertFormFieldToDrawLog,
  DrawLogFormField,
} from './handle';
import { apiDelRound } from '../../../api/round';
import { isNil } from 'lodash';

interface UpdateDrawLogModalProps {
  onFinish: () => void;
  modalOpen: boolean;
  setModalOpen: (open: boolean) => void;
  drawLogEditing: TDrawLog | TDrawLog['index'] | null;
}

export const UpdateDrawLogModal: FC<UpdateDrawLogModalProps> = ({
  onFinish,
  modalOpen,
  setModalOpen,
  drawLogEditing,
}) => {
  const [drawLogVal, setDrawLogVal] = useState<TDrawLog | null>(null);
  useEffect(() => {
    if (drawLogEditing == null) {
      setModalOpen(false);
    } else if (typeof drawLogEditing === 'number') {
      apiGetSelectLog(drawLogEditing)
        .then((res) => {
          setDrawLogVal(res.data);
        })
        .catch((e) => {
          console.error(e);
          message.error('获取抽选信息失败');
          setModalOpen(false);
        });
    } else {
      setDrawLogVal(drawLogEditing);
    }
  }, [drawLogEditing]);

  const [okLoading, setOkLoading] = useState(false);

  const [formInstance, setFormInstance] =
    useState<FormInstance<DrawLogFormField> | null>(null);

  const onModalOk: ModalProps['onOk'] = async () => {
    if (drawLogVal == null) {
      console.error('modal update, drawLogVal is null');
      return;
    }
    try {
      await formInstance?.validateFields();
    } catch (e) {
      console.error(e);
      message.error('请检查表单');
      return;
    }
    const formVal = formInstance?.getFieldsValue();
    if (formVal == null) {
      return;
    }
    console.log('onModalOk', formVal);
    setOkLoading(true);

    // get the round been del, first get the index list of all rounds, eg [0, 1, 2, 3, 4]
    // second, filter out the index of rounds that not in the formVal.rounds, eg [1, 3]
    const roundDelIdxList = Array.from(
      { length: drawLogVal.rounds.length },
      (_, idx) => idx,
    ).filter((idx) => {
      return formVal.rounds == null
        ? true
        : !formVal.rounds.some((round) => round.key === idx);
    });

    try {
      const drawLogConverted = convertFormFieldToDrawLog(formVal);
      await apiUpdateSelectLog({
        index: drawLogConverted.index,
        title: drawLogConverted.title,
        time: drawLogConverted.time,
      });
      message.success('更新成功');
      await Promise.all(
        roundDelIdxList.map((idx) => apiDelRound(drawLogVal.index, idx)),
      );
    } catch (e) {
      // @ts-ignore
      message.error(`更新失败: ${e?.message}`);
    } finally {
      setOkLoading(false);
      setTimeout(() => {
        setModalOpen(false);
        formInstance?.resetFields();
      }, 1000);
      onFinish();
    }
  };
  const onModalCancel: ModalProps['onCancel'] = () => {
    setModalOpen(false);
    formInstance?.resetFields();
  };

  const formInitialValue = useMemo(
    () =>
      isNil(drawLogVal) ? undefined : convertDrawLogToFormField(drawLogVal),
    [drawLogVal],
  );

  const ModalFooter: ModalProps['footer'] = (_) => (
    <>
      <Button onClick={() => formInstance?.resetFields()}>重置</Button>
      <Button onClick={onModalCancel}>取消</Button>
      <Button onClick={onModalOk} loading={okLoading}>
        确认
      </Button>
    </>
  );

  return (
    <Modal
      title="修改抽选记录（表单）"
      open={modalOpen}
      onCancel={onModalCancel}
      footer={ModalFooter}
    >
      <DrawLogForm
        onFormInstanceReady={setFormInstance}
        addOrUpdate={'update'}
        initialValues={formInitialValue}
      />
    </Modal>
  );
};
