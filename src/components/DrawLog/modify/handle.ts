import { IDrawRound, TDrawLog } from '../../../api/handle';
import dayjs, { Dayjs } from 'dayjs';

export type DrawLogFormField = Partial<Pick<TDrawLog, 'index'>> &
  Pick<TDrawLog, 'title'> & {
    time?: Dayjs;
    rounds?: Array<
      {
        key: number;
      } & IDrawRound
    >;
    first_cnt: number;
    second_cnt: number;
  };

export const convertDrawLogToFormField = (
  drawLog: TDrawLog,
): DrawLogFormField => ({
  ...drawLog,
  time: dayjs(drawLog.time),
  rounds: drawLog.rounds.map((round, index) => ({
    key: index, // for pop up round
    ...round,
  })),
});
export const convertFormFieldToDrawLog = (
  formField: DrawLogFormField,
): TDrawLog => ({
  ...formField,
  // 2001-02-10T00:00:00
  index: formField.index ?? -1,
  rounds: formField.rounds
    ? formField.rounds.map((round) => {
        return {
          current_experts: round.current_experts,
          status_list: round.status_list,
          if_log: round.if_log,
        };
      })
    : [],

  time: formField.time?.toISOString() ?? '',
  first_cnt: formField.first_cnt,
  second_cnt: formField.second_cnt,
});
