import { Button, DatePicker, Form, FormInstance, Input } from 'antd';
import React, { FC, useEffect } from 'react';
import { DrawLogFormField } from './handle';
import cn from 'classnames';
import { MinusCircleOutlined } from '@ant-design/icons';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 },
  },
};

const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: { span: 24, offset: 0 },
    sm: { span: 20, offset: 4 },
  },
};

export interface DrawLogFormProps {
  className?: string;
  initialValues?: DrawLogFormField;
  disabled?: boolean;
  onFormInstanceReady: (form: FormInstance<DrawLogFormField>) => void;
  addOrUpdate?: 'add' | 'update';
}

export const DrawLogForm: FC<DrawLogFormProps> = ({
  className,
  initialValues = {
    title: '',
  },
  disabled = false,
  onFormInstanceReady,
  addOrUpdate = 'add',
}) => {
  const [form] = Form.useForm<DrawLogFormField>();
  useEffect(() => form.resetFields(), [initialValues]);
  useEffect(() => onFormInstanceReady(form), [form]);
  return (
    <Form
      className={cn(className)}
      form={form}
      {...formItemLayoutWithOutLabel}
      initialValues={initialValues}
      disabled={disabled}
    >
      <Form.Item<DrawLogFormField>
        label="抽签标题"
        name="title"
        rules={[{ required: true, message: '请输入标题!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item<DrawLogFormField>
        label="第一轮筛选人数"
        name="first_cnt"
        rules={[{ required: true, message: '请输入第一轮筛选人数!' }]}
      >
        <Input type="number" disabled={addOrUpdate === 'update'} />
      </Form.Item>
      <Form.Item<DrawLogFormField>
        label="第二轮筛选人数"
        name="second_cnt"
        rules={[{ required: true, message: '请输入第二轮筛选人数!' }]}
      >
        <Input type="number" disabled={addOrUpdate === 'update'} />
      </Form.Item>
      {addOrUpdate === 'update' && (
        <Form.Item<DrawLogFormField> label="创建日期" name="time">
          <DatePicker showTime disabled />
        </Form.Item>
      )}
      {addOrUpdate === 'update' && (
        <Form.List name={['rounds']}>
          {(fields, { remove }, { errors }) => (
            <>
              {fields.map((field, index) => (
                <Form.Item
                  label={index === 0 ? '已抽选轮' : ''}
                  {...(index === 0
                    ? formItemLayout
                    : formItemLayoutWithOutLabel)}
                  required={false}
                  key={field.key}
                >
                  <Form.Item noStyle>
                    <Input
                      disabled={true}
                      value={'*'.repeat(10)}
                      placeholder="抽选轮索引"
                      style={{ width: '60%' }}
                    />
                  </Form.Item>
                </Form.Item>
              ))}
            </>
          )}
        </Form.List>
      )}
    </Form>
  );
};
