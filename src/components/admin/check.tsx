import React, { FC } from 'react';
import { Form, Input, message, Modal } from 'antd';
import { useAppDispatch, useAppSelector } from '../../store/hook';
import {
  selectAdminCheckCB,
  selectAdminCheckOpen,
  setAdminCheckOpen,
} from '../../store/admin';

type FormField = {
  password: string;
};

export const AdminCheck: FC = () => {
  const dispatch = useAppDispatch();
  const setOpen = (val: boolean) => {
    dispatch(setAdminCheckOpen(val));
  };

  const adminCheckCB = useAppSelector(selectAdminCheckCB);

  const open = useAppSelector(selectAdminCheckOpen);
  const [form] = Form.useForm<FormField>();
  const handleOk = async () => {
    try {
      const values = await form.validateFields();
      if (values.password === '18955981') {
        setOpen(false);
        adminCheckCB && adminCheckCB(true);
      } else {
        message.error('密码错误');
        adminCheckCB && adminCheckCB(false);
      }
    } catch (e) {
      console.error(e);
    }
  };
  return (
    <Modal
      title="审核"
      open={open}
      onOk={handleOk}
      onCancel={() => setOpen(false)}
    >
      <Form
        form={form}
        layout="vertical"
        name="admin-check"
        initialValues={{ remember: true }}
      >
        <Form.Item
          label="密码"
          name="password"
          rules={[{ required: true, message: '请输入密码!' }]}
        >
          <Input.Password />
        </Form.Item>
      </Form>
    </Modal>
  );
};
