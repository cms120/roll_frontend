import { Menu, MenuProps } from 'antd';
import React, { FC, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const items: MenuProps['items'] = [
  {
    label: '首页',
    key: '/',
  },
  // {
  //   label: '后台',
  //   key: '/admin',
  // },
];

export const MenuAppTop: FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const onClick: MenuProps['onClick'] = ({ key }) => {
    // if key is /admin, then confirm to navigate to /admin
    if (key === '/admin') {
      if (window.confirm('确定去后台界面吗?')) {
        navigate(key);
      }
      return;
    }
    navigate(key);
  };
  const [current, setCurrent] = React.useState('');
  useEffect(() => {
    if (location && location.pathname !== current) {
      setCurrent(location.pathname);
    }
  }, [location]);

  return (
    <Menu
      onClick={onClick}
      selectedKeys={[current]}
      mode="horizontal"
      items={items}
    />
  );
};
