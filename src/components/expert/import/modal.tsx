import React, { FC, useEffect, useMemo, useState } from 'react';
import { Button, Modal, ModalProps, Tag } from 'antd';
import {
  ExpertTable,
  getExpertTableActionColumnImport,
  getExpertTableColumnsImport,
} from '../table';
import { TExpert } from '../../../api/handle';
import styles from './index.module.scss';
import cn from 'classnames';

export interface IExpertImportModalProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  expertImported: TExpert['index'][];
  expertsAll: TExpert[];

  onModalOk: (experts: TExpert['index'][]) => void;
}

export const ExpertImportModal: FC<IExpertImportModalProps> = ({
  open,
  setOpen,
  expertImported,
  expertsAll,
  onModalOk,
}) => {
  const [expertsImportedLocal, setExpertsImportedLocal] = useState<
    TExpert['index'][]
  >([]);
  const expertsNotImportLocal = useMemo(
    () =>
      expertsAll.filter(
        (expert) => !expertsImportedLocal.includes(expert.index),
      ),
    [expertsImportedLocal],
  );

  useEffect(() => {
    setExpertsImportedLocal(expertImported);
  }, [expertImported, expertsAll]);

  const handleAddExpert = (key: TExpert['index'] | TExpert['index'][]) => {
    const keys = Array.isArray(key) ? key : [key];
    setExpertsImportedLocal([...expertsImportedLocal, ...keys]);
  };
  const handleRemoveExpert = (key: TExpert['index']) => {
    setExpertsImportedLocal(
      expertsImportedLocal.filter((index) => index !== key),
    );
  };

  const ModalFooter: ModalProps['footer'] = (_) => (
    <>
      <Button onClick={() => setExpertsImportedLocal(expertImported)}>
        重置
      </Button>
      <Button onClick={() => setOpen(false)}>取消</Button>
      <Button onClick={() => onModalOk(expertsImportedLocal)}>确认</Button>
    </>
  );
  return (
    <Modal
      className={cn(styles['expert-import-modal-root'])}
      title="导入专家"
      open={open}
      onCancel={() => setOpen(false)}
      footer={ModalFooter}
      width="80%"
    >
      {expertsImportedLocal.length > 0 && (
        <div className={cn('hstack', 'gap-1', 'flex-wrap')}>
          {expertsImportedLocal.map((index) => {
            return (
              <Tag
                key={index}
                closable={true}
                onClose={() => handleRemoveExpert(index)}
              >
                {expertsAll.find((expert) => expert.index === index)?.name}
              </Tag>
            );
          })}
        </div>
      )}
      <ExpertTable
        experts={expertsNotImportLocal}
        getColumns={(data) =>
          getExpertTableColumnsImport(
            data,
            getExpertTableActionColumnImport(handleAddExpert),
          )
        }
      />
    </Modal>
  );
};
