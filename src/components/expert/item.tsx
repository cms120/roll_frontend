import React, { FC } from 'react';
import { TExpert } from '../../api/handle';

export interface IExpertItemProps {
  expert: TExpert;
}

export const ExpertItem: FC<IExpertItemProps> = ({ expert }) => {
  return <div>ExpertItem</div>;
};
