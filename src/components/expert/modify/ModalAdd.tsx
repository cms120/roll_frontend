import React, { FC, useState } from 'react';
import { Button, FormInstance, Modal, ModalProps } from 'antd';
import { ExpertForm } from './form';
import { convertFormFieldToExpert, ExpertFormFieldType } from './handle';
import { useResetFormOnCloseModal } from '../../../hooks/modal';
import { useAddExpert } from '../../../handle/expert/hook';
import { useValidateSubmitForm } from '../../../hooks/form';
import {
  useExpertListCtx,
  useExpertListDispatch,
} from '../../../handle/ExpertList/ctx';

interface AddExpertModalProps {
  onFinish?: () => void;
}

export const AddExpertModal: FC<AddExpertModalProps> = ({ onFinish }) => {
  const { modalAddOpen: open } = useExpertListCtx();
  const expertsDispatch = useExpertListDispatch();
  const close = () => {
    expertsDispatch({ type: 'setModalAddOpen', payload: false });
  };
  const [form, setForm] = useState<FormInstance<ExpertFormFieldType> | null>(
    null,
  );

  useResetFormOnCloseModal({
    form: form,
    open: open,
  });

  const [onAddExpert] = useAddExpert();
  const [validateSubmit, submitStatus] = useValidateSubmitForm(
    async (formVal) => {
      await onAddExpert(convertFormFieldToExpert(formVal));
    },
    form,
    () =>
      setTimeout(() => {
        close();
        onFinish && onFinish();
      }, 1000),
  );

  const ModalFooter: ModalProps['footer'] = (_) => (
    <>
      <Button onClick={() => form?.resetFields()}>重置</Button>
      <Button onClick={close}>取消</Button>
      <Button onClick={validateSubmit} loading={submitStatus === 'loading'}>
        确认
      </Button>
    </>
  );

  return (
    <Modal
      title="添加专家（表单）"
      open={open}
      onCancel={close}
      footer={ModalFooter}
    >
      <ExpertForm onFormInstanceReady={setForm} />
    </Modal>
  );
};
