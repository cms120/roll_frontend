import React, { FC, useEffect } from 'react';
import { DatePicker, Form, FormInstance, Input, Radio } from 'antd';
import {
  ExpertFormFieldType,
  InsideOutsideOptions,
  LibraryOptions,
} from './handle';

interface IExpertFormProps {
  initialValues?: ExpertFormFieldType;
  onFormInstanceReady: (form: FormInstance<ExpertFormFieldType>) => void;
}

export const ExpertForm: FC<IExpertFormProps> = ({
  initialValues,
  onFormInstanceReady,
}) => {
  const [form] = Form.useForm<ExpertFormFieldType>();

  useEffect(() => {
    console.log('expert form reset fields', initialValues);
    form.resetFields();
  }, [initialValues]);
  useEffect(() => {
    onFormInstanceReady(form);
  }, [form]);
  return (
    <Form form={form} initialValues={initialValues}>
      <Form.Item<ExpertFormFieldType>
        label={LibraryOptions.map((item) => item.label).join('/')}
        name="library"
        rules={[{ required: true, message: '请选择库!' }]}
      >
        <Radio.Group options={LibraryOptions} optionType="button" />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="姓名"
        name="name"
        rules={[{ required: true, message: '请输入姓名!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="出生日期"
        name="birth_date"
        rules={[{ required: true, message: '请输入出生日期!' }]}
      >
        <DatePicker />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="单位"
        name="organization"
        rules={[{ required: true, message: '请输入组织!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="人才称号"
        name="talent_title"
        rules={[{ required: true, message: '请输入人才称号!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label={InsideOutsideOptions.map((item) => item.label).join('/')}
        name="inside_outside"
        rules={[{ required: true, message: '请选择是否校内!' }]}
      >
        <Radio.Group options={InsideOutsideOptions} optionType="button" />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="一级学科"
        name="primary_discipline"
        rules={[{ required: true, message: '请输入一级学科!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="二级学科1"
        name="secondary_discipline1"
        rules={[{ required: true, message: '请输入二级学科1!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="二级学科2"
        name="secondary_discipline2"
        rules={[{ required: true, message: '请输入二级学科2!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="phone_number"
        name="phone_number"
        rules={[{ required: true, message: '请输入电话号码!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item<ExpertFormFieldType>
        label="email"
        name="email"
        rules={[{ required: true, message: '请输入邮箱!' }]}
      >
        <Input />
      </Form.Item>
    </Form>
  );
};
