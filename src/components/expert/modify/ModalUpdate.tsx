import React, { FC, useMemo, useState } from 'react';
import { Button, FormInstance, message, Modal, ModalProps } from 'antd';
import { ExpertForm } from './form';
import {
  convertExpertToFormField,
  convertFormFieldToExpert,
  ExpertFormFieldType,
} from './handle';
import { useUpdateExpert } from '../../../handle/expert/hook';
import { useValidateSubmitForm } from '../../../hooks/form';
import {
  useExpertListCtx,
  useExpertListDispatch,
} from '../../../handle/ExpertList/ctx';
import { cloneDeep, isNil } from 'lodash';

interface UpdateExpertModalProps {
  onFinish?: () => void;
}

export const UpdateExpertModal: FC<UpdateExpertModalProps> = ({ onFinish }) => {
  const { modalUpdateOpen: open, rowEditing: expertEditing } =
    useExpertListCtx();
  const expertsDispatch = useExpertListDispatch();
  const close = () => {
    expertsDispatch({ type: 'setModalUpdateOpen', payload: false });
  };
  const [form, setForm] = useState<FormInstance<ExpertFormFieldType> | null>(
    null,
  );

  const [onUpdateExpert] = useUpdateExpert();
  const [validateSubmit, submitStatus] = useValidateSubmitForm(
    async (formVal) => {
      console.log('form val', formVal);
      if (isNil(expertEditing)) {
        console.error('modal update, expertEditing is null');
        message.error('专家信息为空');
        return;
      }
      await onUpdateExpert({
        ...convertFormFieldToExpert(formVal),
        index: expertEditing.index,
      });
    },
    form,
    () =>
      setTimeout(() => {
        close();
        onFinish && onFinish();
      }, 1000),
  );

  const ModalFooter: ModalProps['footer'] = (_) => (
    <>
      <Button onClick={() => form?.resetFields()}>重置</Button>
      <Button onClick={close}>取消</Button>
      <Button onClick={validateSubmit} loading={submitStatus === 'loading'}>
        确认
      </Button>
    </>
  );

  const formInitialValue = useMemo(() => {
    return expertEditing
      ? convertExpertToFormField(cloneDeep(expertEditing))
      : undefined;
  }, [expertEditing]);

  return (
    <Modal
      title="更新专家（表单）"
      open={open}
      onCancel={close}
      footer={ModalFooter}
    >
      <ExpertForm
        onFormInstanceReady={setForm}
        initialValues={formInitialValue}
      />
    </Modal>
  );
};
