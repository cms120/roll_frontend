import { TExpert } from '../../../api/handle';
import dayjs, { Dayjs } from 'dayjs';

export type ExpertFormFieldType = Omit<TExpert, 'birth_date' | 'index'> & {
  birth_date: Dayjs;
};
export const ExpertFormDefault: ExpertFormFieldType = {
  library: 'A',
  name: '',
  birth_date: dayjs(),
  organization: '',
  talent_title: '',
  inside_outside: '校内',
  primary_discipline: '',
  secondary_discipline1: '',
  secondary_discipline2: '',
  phone_number: '',
  email: '',
};
export const convertExpertToFormField = (
  expert: TExpert,
): ExpertFormFieldType => ({
  ...expert,
  birth_date: dayjs(expert.birth_date),
});
export const convertFormFieldToExpert = (
  formField: ExpertFormFieldType,
): Omit<TExpert, 'index'> => ({
  ...formField,
  // 2001-02-10T00:00:00
  birth_date: formField.birth_date.toISOString(),
});
export const LibraryOptions = [
  { label: 'A库', value: 'A' },
  { label: 'B库', value: 'B' },
  { label: 'C库', value: 'C' },
];
export const InsideOutsideOptions = [
  { label: '校内', value: '校内' },
  { label: '校外', value: '校外' },
];
