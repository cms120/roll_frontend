import React, { FC, useMemo, useState } from 'react';
import { Table, TableProps } from 'antd';
import type { TableRowSelection } from 'antd/es/table/interface';
import styles from './index.module.scss';
import { IExpertTableField, IExpertTableProps } from './handle';
import cn from 'classnames';

export const ExpertTable: FC<IExpertTableProps> = ({
  experts,
  statusList,
  loading,
  title = () => null,
  getColumns,
}) => {
  const dataSource = useMemo(
    () =>
      experts.map((expert, index) => ({
        ...expert,
        key: expert.index,
        status: statusList?.[index],
      })),
    [experts],
  );

  const columns: TableProps<IExpertTableField>['columns'] = useMemo(
    () => getColumns(dataSource),
    [dataSource],
  );

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const onSelectChange: TableRowSelection<IExpertTableField>['onChange'] = (
    newSelectedRowKeys: React.Key[],
  ) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  return (
    <Table
      className={cn(styles['expert-table-root'], 'expert-table-root')}
      columns={columns}
      title={(data) => title(data, selectedRowKeys)}
      dataSource={dataSource}
      loading={loading}
      bordered
      rowSelection={{
        type: 'checkbox',
        selectedRowKeys,
        selections: true,
        onChange: onSelectChange,
      }}
    />
  );
};
