import { TDrawStatus, TExpert } from '../../../api/handle';
import React from 'react';
import { TableProps } from 'antd';

export interface IExpertTableField extends TExpert {
  key: React.Key;
  status?: TDrawStatus;
}

export type TExpertTableColumn = NonNullable<
  TableProps<IExpertTableField>['columns']
>[number];

export interface IExpertTableProps {
  experts: TExpert[];
  getColumns: (dataSource: IExpertTableField[]) => TExpertTableColumn[];

  statusList?: TDrawStatus[];
  loading?: boolean;
  title?: (
    data: Readonly<IExpertTableField[]>,
    selectedRowKeys: React.Key[],
  ) => React.ReactNode;
}
