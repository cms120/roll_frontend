import { Button, Popconfirm } from 'antd';
import cn from 'classnames';
import React, { FC } from 'react';

export interface IExpertTableTitleDrawProps {
  onAdd: () => void;
  onRemove: () => void;
  delBtnDisabled: boolean;
  expertsCount: number;
}

export const ExpertTableTitleDrawRoundFirst: FC<IExpertTableTitleDrawProps> = ({
  onAdd,
  onRemove,
  delBtnDisabled,
  expertsCount,
}) => (
  <div
    className={cn(
      'hstack',
      'gap-2',
      'justify-content-center',
      'position-relative',
    )}
  >
    {/*<Button type="primary" onClick={onAdd}>*/}
    {/*  添加抽选*/}
    {/*</Button>*/}
    <Popconfirm
      title="确认移除?"
      onConfirm={onRemove}
      okText="确认"
      cancelText="取消"
    >
      <Button
        className={cn('position-absolute', 'start-0')}
        type="primary"
        danger
        disabled={delBtnDisabled}
      >
        移除抽选
      </Button>
    </Popconfirm>
    <span className={cn('fs-3')}>{`专家列表，当前${expertsCount}人`}</span>
  </div>
);
