import { Button, Popconfirm } from 'antd';
import cn from 'classnames';
import React, { FC } from 'react';

export interface IExpertTableTitleDrawProps {
  onAdd: () => void;
  onRemove: () => void;
  delBtnDisabled: boolean;
  expertsCount: number;
}

export const ExpertTableTitleDrawRoundSecond: FC<
  IExpertTableTitleDrawProps
> = ({ onAdd, onRemove, delBtnDisabled, expertsCount }) => (
  <div
    className={cn(
      'hstack',
      'gap-2',
      'justify-content-center',
      'position-relative',
    )}
  >
    {/*<Button type="primary" onClick={onAdd}>*/}
    {/*  添加抽选*/}
    {/*</Button>*/}
    <div className={cn('position-absolute', 'start-0', 'hstack', 'gap-1')}>
      <Popconfirm
        title="确认移除?"
        onConfirm={onRemove}
        okText="确认"
        cancelText="取消"
      >
        <Button type="primary" danger disabled={delBtnDisabled}>
          移除抽选
        </Button>
      </Popconfirm>
      <Popconfirm title={'确认发短信？'} okText="确认" cancelText="取消">
        <Button type="primary" disabled={delBtnDisabled}>
          发短信
        </Button>
      </Popconfirm>
    </div>

    <span className={cn('fs-3')}>{`专家列表，当前${expertsCount}人`}</span>
  </div>
);
