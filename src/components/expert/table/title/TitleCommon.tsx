import { Button, Popconfirm } from 'antd';
import cn from 'classnames';
import React, { FC } from 'react';

export interface IExpertTableTitleCommonProps {
  onDel: () => void;
  delBtnDisabled?: boolean;
  delBtnLoading?: boolean;
}

export const ExpertTableTitleCommon: FC<IExpertTableTitleCommonProps> = ({
  onDel,
  delBtnDisabled,
  delBtnLoading,
}) => {
  return (
    <div className={cn('hstack', 'gap-1')}>
      <span>专家列表</span>
      <Popconfirm
        title="确认删除?"
        onConfirm={onDel}
        okText="确认"
        cancelText="取消"
      >
        <Button
          type="primary"
          danger
          disabled={delBtnDisabled}
          loading={delBtnLoading}
        >
          删除
        </Button>
      </Popconfirm>
    </div>
  );
};
