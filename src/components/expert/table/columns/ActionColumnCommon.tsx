import cn from 'classnames';
import { Button, Popconfirm } from 'antd';
import React from 'react';
import { TExpertTableColumn } from '../handle';

export const getExpertTableActionColumnCommon = (
  onEditRow: (key: React.Key) => void,
  onDelRow: (key: React.Key) => void,
): TExpertTableColumn => {
  return {
    title: '操作',
    key: 'operation',
    render: (_, record) => (
      <div className={cn('hstack', 'gap-1')}>
        <Button type="primary" onClick={() => onEditRow(record.key)}>
          编辑
        </Button>
        <Popconfirm
          title="确认删除?"
          onConfirm={() => onDelRow(record.key)}
          okText="确认"
          cancelText="取消"
        >
          <Button type="primary" danger>
            删除
          </Button>
        </Popconfirm>
      </div>
    ),
  };
};
