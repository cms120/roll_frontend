import { IExpertTableField, TExpertTableColumn } from '../handle';
import { InsideOutsideOptions, LibraryOptions } from '../../modify/handle';
import { formatTimeStr } from '../../../../common/timeUtil';
import _ from 'lodash';
import React from 'react';
import { DrawStatusText, TDrawStatus } from '../../../../api/handle';
import dayjs from 'dayjs';

export const getExpertTableStatusColumn = (
  dataSource: IExpertTableField[],
): TExpertTableColumn => {
  const statusFilters: { text: string; value: TDrawStatus }[] = [];
  dataSource.forEach((expert) => {
    if (
      expert.status &&
      !statusFilters.some((item) => item.value === expert.status)
    ) {
      statusFilters.push({
        text: DrawStatusText[expert.status],
        value: expert.status,
      });
    }
  });
  return {
    title: '状态',
    dataIndex: 'status',
    key: 'status',
    render: (val, record) => (
      <span>{record.status ? DrawStatusText[record.status] : '未知状态'}</span>
    ),
    filters: statusFilters,
    onFilter: (value, record) => record.status === value,
  };
};
export const ExpertTableIndexColumn: TExpertTableColumn = {
  title: '序号',
  dataIndex: 'index',
  key: 'index',
  showSorterTooltip: { target: 'full-header' },
  sorter: (a, b) => a.index - b.index,
  sortDirections: ['ascend', 'descend'],
};
export const ExpertTableNameColumn: TExpertTableColumn = {
  title: '姓名',
  dataIndex: 'name',
  key: 'name',
  showSorterTooltip: { target: 'full-header' },
  /**
   * sort the data by character ascii code,
   * when first is equally, compare the second, if second is equally, compare the third, and so on.
   * if the first is equally, and length of a or b is 1, then return the short 1
   * @param a
   * @param b
   */
  sorter: (a, b) => {
    return a.name.localeCompare(b.name);
  },
  sortDirections: ['descend', 'ascend'],
};

export const getExpertTableInfoColumns = (
  dataSource: IExpertTableField[],
): TExpertTableColumn[] => {
  return [
    {
      title: 'A/B/C库',
      dataIndex: 'library',
      key: 'library',
      filters: LibraryOptions.map((item) => ({
        text: item.label,
        value: item.value,
      })),
      onFilter: (value, record) => record.library === value,
    },
    {
      title: '出生日期',
      dataIndex: 'birth_date',
      key: 'birth_date',
      render: (val) => {
        const date = dayjs(val);
        let age = 0;
        if (date.isValid()) {
          age = dayjs().diff(date, 'year');
        }
        return `${formatTimeStr(val)} (${age}岁)`;
      },
      showSorterTooltip: { target: 'full-header' },
      sorter: (a, b) => {
        return dayjs(a.birth_date).diff(dayjs(b.birth_date));
      },
      sortDirections: ['ascend', 'descend'],
    },
    {
      title: '单位',
      dataIndex: 'organization',
      key: 'organization',
      filters: _.uniq(dataSource.map((expert) => expert.organization)).map(
        (item) => ({
          text: item,
          value: item,
        }),
      ),
      onFilter: (value, record) => record.organization === value,
    },
    {
      title: '职称',
      dataIndex: 'talent_title',
      key: 'talent_title',
      filters: _.uniq(dataSource.map((expert) => expert.talent_title)).map(
        (item) => ({
          text: item,
          value: item,
        }),
      ),
      onFilter: (value, record) => record.talent_title === value,
    },
    {
      title: '校内/校外',
      dataIndex: 'inside_outside',
      key: 'inside_outside',
      filters: InsideOutsideOptions.map((item) => ({
        text: item.label,
        value: item.value,
      })),
      onFilter: (value, record) => record.inside_outside === value,
    },
    {
      title: '一级学科',
      dataIndex: 'primary_discipline',
      key: 'primary_discipline',
      filters: _.uniq(
        dataSource.map((expert) => expert.primary_discipline),
      ).map((item) => ({
        text: item,
        value: item,
      })),
      onFilter: (value, record) => record.primary_discipline === value,
    },
    {
      title: '二级学科1',
      dataIndex: 'secondary_discipline1',
      key: 'secondary_discipline1',
    },
    {
      title: '二级学科2',
      dataIndex: 'secondary_discipline2',
      key: 'secondary_discipline2',
    },
    {
      title: '电话',
      dataIndex: 'phone_number',
      key: 'phone_number',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
    },
  ];
};
export const getExpertTableColumnsCommon = (
  dataSource: IExpertTableField[],
  actionColumn: TExpertTableColumn,
): TExpertTableColumn[] => [
  ExpertTableNameColumn,
  actionColumn,
  ...getExpertTableInfoColumns(dataSource),
];

export const getExpertTableColumnsDrawRoundFirst = (
  dataSource: IExpertTableField[],
  actionColumn: TExpertTableColumn,
): TExpertTableColumn[] => [
  ExpertTableNameColumn,
  actionColumn,
  ...getExpertTableInfoColumns(dataSource),
];

export const getExpertTableColumnsDrawRoundSecond = (
  dataSource: IExpertTableField[],
  actionColumn: TExpertTableColumn,
): TExpertTableColumn[] => [
  ExpertTableNameColumn,
  actionColumn,
  {
    title: '状态',
    dataIndex: 'status',
    key: 'status',
    render: (val, record) => <span>{'待发短信'}</span>,
    onFilter: (value, record) => record.status === value,
  },
  ...getExpertTableInfoColumns(dataSource),
];
export const getExpertTableColumnsImport = (
  dataSource: IExpertTableField[],
  actionColumn: TExpertTableColumn,
): TExpertTableColumn[] => [
  ExpertTableNameColumn,
  actionColumn,
  getExpertTableStatusColumn(dataSource),
];
