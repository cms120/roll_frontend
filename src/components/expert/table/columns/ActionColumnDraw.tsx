import cn from 'classnames';
import { Button, Popconfirm } from 'antd';
import React from 'react';
import { TExpertTableColumn } from '../handle';

export const getExpertTableActionColumnDrawRoundFirst = (
  onRemoveRow: (key: React.Key) => void,
  disabled?: boolean,
): TExpertTableColumn => {
  return {
    title: '操作',
    key: 'operation',
    render: (_, record) => (
      <div className={cn('hstack', 'gap-1')}>
        <Popconfirm
          title="确认删除?"
          onConfirm={() => onRemoveRow(record.key)}
          okText="确认"
          cancelText="取消"
        >
          <Button type="primary" disabled={disabled} danger>
            移除抽选
          </Button>
        </Popconfirm>
      </div>
    ),
  };
};

export const getExpertTableActionColumnDrawRoundSecond = (
  onRemoveRow: (key: React.Key) => void,
  onTextRow: (key: React.Key) => void,
  disabled?: boolean,
): TExpertTableColumn => {
  return {
    title: '操作',
    key: 'operation',
    render: (_, record) => (
      <div className={cn('hstack', 'gap-1')}>
        <Popconfirm
          title="确认删除?"
          onConfirm={() => onRemoveRow(record.key)}
          okText="确认"
          cancelText="取消"
        >
          <Button type="primary" disabled={disabled} danger>
            移除抽选
          </Button>
        </Popconfirm>
        <Popconfirm
          title={'确认发短信？'}
          onConfirm={() => onTextRow(record.key)}
          okText="确认"
          cancelText="取消"
        >
          <Button type="primary" disabled={disabled}>
            发短信
          </Button>
        </Popconfirm>
      </div>
    ),
  };
};
