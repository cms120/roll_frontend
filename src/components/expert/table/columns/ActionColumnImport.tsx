import cn from 'classnames';
import { Button } from 'antd';
import React from 'react';
import { TExpertTableColumn } from '../handle';
import { TExpert } from '../../../../api/handle';

export const getExpertTableActionColumnImport = (
  onAddRow: (key: TExpert['index']) => void,
): TExpertTableColumn => {
  return {
    title: '操作',
    key: 'operation',
    render: (_, record) => (
      <div className={cn('hstack', 'gap-1')}>
        <Button type="primary" onClick={() => onAddRow(record.index)}>
          添加
        </Button>
      </div>
    ),
  };
};
