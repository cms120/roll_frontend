import React, { FC, useState } from 'react';
import { Button, FormInstance, Modal, ModalProps } from 'antd';
import { ExpertLoadForm, ExpertLoadFormFieldType } from './form';
import {
  useExpertListCtx,
  useExpertListDispatch,
} from '../../../handle/ExpertList/ctx';
import { useResetFormOnCloseModal } from '../../../hooks/modal';

interface ExpertModalLoadProps {
  onFinish?: () => void;
}

export const ExpertLoadModal: FC<ExpertModalLoadProps> = ({ onFinish }) => {
  const { modalLoadOpen: open } = useExpertListCtx();
  const expertsDispatch = useExpertListDispatch();
  const close = () => {
    expertsDispatch({ type: 'setModalLoadOpen', payload: false });
  };

  const [form, setForm] =
    useState<FormInstance<ExpertLoadFormFieldType> | null>(null);

  useResetFormOnCloseModal({
    form: form,
    open: open,
  });

  const ModalFooter: ModalProps['footer'] = (_) => (
    <Button onClick={close}>取消</Button>
  );

  const onFinishLocal = () => {
    onFinish && onFinish();
    close();
  };

  return (
    <Modal
      title="导入专家（文件）"
      open={open}
      onCancel={close}
      footer={ModalFooter}
    >
      <ExpertLoadForm onFormInstanceReady={setForm} onFinish={onFinishLocal} />
    </Modal>
  );
};
