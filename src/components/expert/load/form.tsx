import React, { FC, useEffect } from 'react';
import { Button, Form, FormInstance, message, Upload, UploadProps } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUpload } from '@fortawesome/free-solid-svg-icons';
import { apiImportExperts } from '../../../api/expert';

export type ExpertLoadFormFieldType = {
  excel_file: File;
};

export interface ExpertLoadFormProps {
  initialValues?: ExpertLoadFormFieldType;
  onFormInstanceReady: (form: FormInstance<ExpertLoadFormFieldType>) => void;
  onFinish: () => void;
}

export const ExpertLoadForm: FC<ExpertLoadFormProps> = ({
  initialValues,
  onFormInstanceReady,
  onFinish,
}) => {
  const [form] = Form.useForm<ExpertLoadFormFieldType>();
  useEffect(() => {
    form.resetFields();
  }, [initialValues]);
  useEffect(() => {
    onFormInstanceReady(form);
  }, [form]);

  const customRequest: UploadProps['customRequest'] = async ({
    file,
    onError,
    onSuccess,
    onProgress,
  }) => {
    onProgress &&
      onProgress({
        percent: 0,
      });
    if (file == null) {
      return;
    }
    try {
      await apiImportExperts({
        excel_file: file as File,
      });

      onProgress &&
        onProgress({
          percent: 100,
        });
      message.success('添加成功');
      onFinish();
      onSuccess && onSuccess(null);
    } catch (e) {
      let msg = '';
      if (e instanceof Object && 'message' in e) {
        msg = String(e.message);
      }
      message.error(`添加失败: ${msg}`);
      onError &&
        onError({
          name: 'error',
          message: msg,
        });
    }
  };

  return (
    <Form form={form}>
      <Form.Item<ExpertLoadFormFieldType>
        label="file"
        name="excel_file"
        rules={[{ required: true, message: 'Please select file!' }]}
      >
        <Upload
          maxCount={1}
          accept=".csv , .xlsx , .xls"
          customRequest={customRequest}
        >
          <Button icon={<FontAwesomeIcon icon={faUpload} />}>点击上传</Button>
        </Upload>
      </Form.Item>
    </Form>
  );
};
