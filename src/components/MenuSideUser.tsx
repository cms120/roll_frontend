import { Menu, MenuProps } from 'antd';
import React, { FC, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const items: MenuProps['items'] = [
  {
    label: '专家列表',
    key: '/',
  },
  {
    label: '开始抽签',
    key: '/draw',
  },
  {
    label: '抽签记录',
    key: '/drawList',
  },
];

export const MenuSideUser: FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const onClick: MenuProps['onClick'] = ({ key }) => {
    navigate(key);
  };
  const [current, setCurrent] = React.useState('');
  useEffect(() => {
    if (!(location && location.pathname !== current)) {
      return;
    }
    const pathname = location.pathname;
    if (pathname.startsWith('/draw/') && pathname.startsWith('/draw?')) {
      setCurrent('/draw');
    } else {
      setCurrent(pathname);
    }
  }, [location]);

  return (
    <Menu
      onClick={onClick}
      selectedKeys={[current]}
      mode="vertical"
      items={items}
    />
  );
};
