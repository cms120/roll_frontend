import { Steps } from 'antd';
import React, { FC } from 'react';
import { stepsItems } from './handle';
import cn from 'classnames';
import styles from './index.module.scss';

interface IDrawStepsProps {
  current: number;
  className?: string;
}

export const DrawSteps: FC<IDrawStepsProps> = ({ current, className }) => {
  return (
    <Steps
      rootClassName={cn(styles['draw-steps-root'], className)}
      className={cn(styles['draw-step-nav'])}
      current={current}
      items={stepsItems}
    />
  );
};
