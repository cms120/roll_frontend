import { Button, message } from 'antd';
import React, { FC } from 'react';
import { stepsItems } from './handle';
import cn from 'classnames';

interface IDrawStepsControlProps {
  current: number;
  setCurrent: (current: number) => void;
  stepSuccess: number; // the state index has succeed
  onFinish: () => void;
}

export const DrawStepsControl: FC<IDrawStepsControlProps> = ({
  current,
  setCurrent,
  stepSuccess,
  onFinish,
}) => {
  const next = () => {
    if (current > stepSuccess) {
      message.error('请按顺序完成');
    } else {
      setCurrent(current + 1);
    }
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  return (
    <div className={cn('hstack', 'gap-1')}>
      {current < stepsItems.length - 1 && (
        <Button type="primary" onClick={() => next()}>
          下一步
        </Button>
      )}
      {current === stepsItems.length - 1 && (
        <Button type="primary" onClick={onFinish}>
          完成
        </Button>
      )}
      {current > 0 && (
        <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
          上一步
        </Button>
      )}
    </div>
  );
};
