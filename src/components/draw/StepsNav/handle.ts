import { StepsProps } from 'antd';

export const stepsItems: NonNullable<StepsProps['items']> = [
  {
    title: '创建抽签',
    description: '',
  },
  {
    title: '初次筛选',
    description: '',
  },
  {
    title: '终次筛选',
    description: '',
  },
  {
    title: '抽签结果',
    description: '',
  },
];
