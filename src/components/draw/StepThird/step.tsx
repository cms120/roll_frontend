import React, { FC } from 'react';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import {
  selectDrawLog,
  selectDrawRoundSecond,
  selectExpertsDrewRoundFirst,
  thunkFetchDrawLog,
} from '../../../store/drawing';
import { DrawStepThirdComp } from './StepComp';
import { useUpdateRound } from '../../../handle/round/hook';

export const DrawStepThird: FC = () => {
  const dispatch = useAppDispatch();

  const drawLogNow = useAppSelector(selectDrawLog);
  const drawRoundSecond = useAppSelector(selectDrawRoundSecond);

  // 第一轮抽选的 专家 id
  const expertsIdxDrewRoundFirst = useAppSelector(selectExpertsDrewRoundFirst);

  const handleRefresh = () => {
    if (drawLogNow != null) {
      dispatch(thunkFetchDrawLog(drawLogNow.index));
    }
  };

  const [onUpdateRound] = useUpdateRound();

  if (drawLogNow == null) {
    return <span>选举记录为空 error</span>;
  }
  return (
    <DrawStepThirdComp
      drawLog={drawLogNow}
      drawRound={drawRoundSecond}
      expertsQualifiedInitial={expertsIdxDrewRoundFirst}
      drawCnt={drawLogNow.second_cnt}
      onUpdateRound={async (round) => {
        await onUpdateRound(drawLogNow.index, 1, round);
        handleRefresh();
      }}
    />
  );
};
