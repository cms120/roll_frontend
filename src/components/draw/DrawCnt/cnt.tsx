import { Input, InputProps } from 'antd';
import React, { FC } from 'react';
import styles from './index.module.scss';
import cn from 'classnames';

export interface IDrawNumFirstProps {
  disabled?: boolean;
  onChange?: InputProps['onChange'];
  value: number;
}

export const DrawCntFrom: FC<IDrawNumFirstProps> = ({
  disabled,
  onChange,
  value,
}) => {
  return (
    <Input
      className={cn(styles['draw-num-root'])}
      value={value}
      disabled={disabled}
      addonBefore={'初筛环节抽取'}
      addonAfter={'人'}
      type="number"
      onChange={onChange}
    />
  );
};
