import React, { FC } from 'react';
import { Button } from 'antd';
import { IDrawRound } from '../../../api/handle';
import cn from 'classnames';

export interface IDrawLockBtnProps {
  onChange: (val: IDrawRound['if_log']) => void;
  lockBtnDisabled?: boolean;
  unlockBtnDisabled?: boolean;
}

export const DrawLockBtn: FC<IDrawLockBtnProps> = ({
  onChange,
  lockBtnDisabled,
  unlockBtnDisabled,
}) => (
  <div className={cn('hstack', 'gap-1')}>
    <Button onClick={() => onChange(1)} disabled={lockBtnDisabled}>
      锁定本轮
    </Button>
    <Button onClick={() => onChange(2)} disabled={unlockBtnDisabled}>
      解锁本轮
    </Button>
  </div>
);
