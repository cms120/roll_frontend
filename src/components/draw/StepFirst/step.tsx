import React, { FC, useEffect, useMemo, useState } from 'react';
import cn from 'classnames';
import { DrawLogForm, DrawLogFormField } from '../../DrawLog';
import { Button, FormInstance } from 'antd';
import { useAppSelector } from '../../../store/hook';
import styles from './index.module.scss';
import { useNavigate } from 'react-router-dom';
import { selectDrawLog } from '../../../store/drawing';
import { useAddSelectLog } from '../../../handle/draw/hook';
import { useValidateSubmitForm } from '../../../hooks/form';
import { isNil } from 'lodash';

export const DrawStepFirst: FC = () => {
  const drawLog = useAppSelector(selectDrawLog);

  const navigate = useNavigate();

  const [formInstance, setFormInstance] =
    useState<FormInstance<DrawLogFormField> | null>(null);

  const [onAddSelectLog, addSelectLogStatus, addSelectLogRes] =
    useAddSelectLog();
  const [validateSubmit, submitStatus] = useValidateSubmitForm(
    async (formVal) => {
      await onAddSelectLog({
        ...formVal,
        time: new Date().toISOString(),
      });
    },
    formInstance,
    () => {
      setTimeout(() => {
        formInstance?.resetFields();
      }, 1000);
    },
  );

  useEffect(() => {
    if (addSelectLogStatus === 'success' && !isNil(addSelectLogRes)) {
      const drawLogRes = addSelectLogRes[addSelectLogRes.length - 1];
      const searchParams = new URLSearchParams();
      searchParams.set('current', '1');
      navigate(`/draw/${drawLogRes.index}/?${searchParams.toString()}`);
    }
  }, [addSelectLogStatus, addSelectLogRes]);

  const formInitialValues = useMemo(
    () =>
      drawLog == null
        ? {
            title: '',
            first_cnt: 80,
            second_cnt: 20,
          }
        : {
            title: drawLog.title,
            first_cnt: drawLog.first_cnt,
            second_cnt: drawLog.second_cnt,
          },
    [drawLog],
  );
  return (
    <div className={cn(styles['draw-step-first-root'], 'vstack', 'gap-2')}>
      <div
        className={cn(
          'vstack',
          'draw-log-form-wrapper',
          'p-2',
          'align-self-center',
        )}
      >
        <DrawLogForm
          className={'draw-log-form'}
          initialValues={formInitialValues}
          disabled={drawLog != null}
          onFormInstanceReady={setFormInstance}
        />
        <div className={cn('hstack', 'gap-2')}>
          <Button
            onClick={() => formInstance?.resetFields()}
            disabled={drawLog != null}
          >
            重置
          </Button>
          <Button
            loading={submitStatus === 'loading'}
            onClick={validateSubmit}
            disabled={drawLog != null}
          >
            确认
          </Button>
        </div>
      </div>
    </div>
  );
};
