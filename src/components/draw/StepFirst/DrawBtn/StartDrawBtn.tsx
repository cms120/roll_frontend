import React, { FC } from 'react';
import cn from 'classnames';
import { Button, Popconfirm } from 'antd';
import { useAppSelector } from '../../../../store/hook';
import { IDrawRound } from '../../../../api/handle';
import { selectDrawLog } from '../../../../store/drawing';

const DrawBtn: FC<{
  onClick?: () => void;
  disabled?: boolean;
  content: string;
}> = ({ onClick, disabled, content }) => {
  const drawLogNow = useAppSelector(selectDrawLog);
  return (
    <Button
      onClick={onClick}
      type="primary"
      disabled={disabled ? disabled : drawLogNow == null}
    >
      {content}
    </Button>
  );
};

export interface IDrawBtnProps {
  drawRound?: IDrawRound;
  disabled?: boolean;
  onDraw: () => void;
}

export const StartDrawBtn: FC<IDrawBtnProps> = ({
  drawRound,
  disabled,
  onDraw,
}) => {
  return (
    <div className={cn('hstack', 'gap-2', 'justify-content-center')}>
      <DrawBtn
        onClick={onDraw}
        disabled={disabled || drawRound != null}
        content={'开始抽签'}
      />
      <Popconfirm
        title={'已经抽取过一次，继续将覆盖当前结果！'}
        onConfirm={onDraw}
        okText="确认"
        cancelText="取消"
      >
        <DrawBtn
          disabled={disabled || drawRound == null}
          content={'重新抽签'}
        />
      </Popconfirm>
    </div>
  );
};
