import React, { FC } from 'react';
import cn from 'classnames';
import { Button, Input, InputProps, message } from 'antd';
import styles from './index.module.scss';

export interface ISupplyDrawBtnProps {
  disabled?: boolean;
  /**
   *
   * @param num 要抽取的专家数量 有可能是总数 也有可能是增加的数量
   * @param numIfAdd 是否是添加专家 默认是false 即专家总数
   */
  onDraw: (num: number, numIfAdd: boolean) => void;
}

export const SupplyDrawBtn: FC<ISupplyDrawBtnProps> = ({
  disabled,
  onDraw,
}) => {
  const [pelNum, setPelNum] = React.useState<number>(0);
  const handleChange: InputProps['onChange'] = (e) => {
    setPelNum(Number(e.target.value));
  };

  const handleSupplyBtnClick = () => {
    if (pelNum === 0) {
      message.error('请输入人数!');
    } else {
      onDraw(pelNum, false);
    }
  };
  return (
    <div
      className={cn(
        styles['supply-draw-btn-root'],
        'hstack',
        'gap-3',
        'align-items-center',
        'justify-content-center',
      )}
    >
      <div className={cn('hstack', 'gap-1', 'justify-content-center')}>
        <Button disabled={disabled} onClick={handleSupplyBtnClick}>
          增补抽签
        </Button>
        <Input
          rootClassName={cn('per-input')}
          disabled={disabled}
          onChange={handleChange}
          type={'number'}
          addonBefore={'到'}
          addonAfter={'人'}
        />
      </div>
      <Button disabled={disabled} onClick={() => onDraw(1, true)}>
        递补抽签
      </Button>
    </div>
  );
};
