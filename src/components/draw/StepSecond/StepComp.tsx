import { useAppDispatch, useAppSelector } from '../../../store/hook';
import { selectExpertsSome } from '../../../store/expert';
import { thunkFetchDrawLog } from '../../../store/drawing';
import React, { FC, useState } from 'react';
import { TDrawLog, IDrawRound, TExpert } from '../../../api/handle';
import { apiAddRound, apiUpdateRound } from '../../../api/round';
import { message } from 'antd';
import {
  createDrawRoundByExperts,
  updateDrawRoundExperts,
} from '../../../common/Draw';
import {
  getExpertsDrewFromDrawRound,
  getExpertsQualifiedToBeDrew,
  getExpertsStatusDrewFromDrawRound,
  importDrawRoundExperts,
  removeDrawRoundExperts,
  selectRandomExperts,
} from '../../../common/Draw/DrawLog';
import {
  ISupplyDrawBtnProps,
  SupplyDrawBtn,
} from '../StepFirst/DrawBtn/SupplyDrawBtn';
import {
  ExpertImportModal,
  ExpertTable,
  ExpertTableTitleDrawRoundFirst,
  getExpertTableActionColumnDrawRoundFirst,
  getExpertTableColumnsDrawRoundFirst,
  IExpertTableProps,
} from '../../expert';
import cn from 'classnames';
import { DrawLockBtn, IDrawLockBtnProps } from '../Lock/DrawLockBtn';
import { StartDrawBtn } from '../StepFirst/DrawBtn/StartDrawBtn';
import { DrawCntFrom } from '../DrawCnt';
import { setAdminCheckOpen, setCheckCB } from '../../../store/admin';
import { delay } from 'lodash';
import { useAddRound } from '../../../handle/round/hook';
import { delaySafe } from '../../../common/commonUtil';

export interface IDrawStepsFirstCompProps {
  drawLog: TDrawLog;
  drawRound: IDrawRound | null;
  expertsQualifiedInitial: TExpert['index'][]; // 本轮可以选的所有专家，只在初次抽选时有用
  drawCnt: number; // 本轮选举人数 只在初次选举时有用
  onUpdateRound: (roundNew: IDrawRound) => void;
}

export const DrawStepFirstComp: FC<IDrawStepsFirstCompProps> = ({
  drawLog,
  drawRound,
  expertsQualifiedInitial,
  drawCnt,
  onUpdateRound,
}) => {
  const dispatch = useAppDispatch();

  const ifLock = drawRound?.if_log ?? 0;

  const expertsIdxRoundFirst =
    drawRound == null ? [] : drawRound.current_experts;
  // 有资格参加抽选的专家 如果是第一轮则是所有专家 否则根据 round 筛选
  const expertsIdxQualified =
    drawRound == null
      ? expertsIdxRoundFirst
      : getExpertsQualifiedToBeDrew(drawRound);

  // 抽选出来的专家
  const expertsIdxDrewRound =
    drawRound == null ? [] : getExpertsDrewFromDrawRound(drawRound);
  const expertsStatusDrewRound =
    drawRound == null ? [] : getExpertsStatusDrewFromDrawRound(drawRound);

  const [expertImportModalOpen, setExpertImportModalOpen] = useState(false);

  // 有资格参加抽选的专家 如果是第一轮则是所有专家 否则根据 round 筛选
  const expertsQualifiedRound = useAppSelector((state) =>
    selectExpertsSome(state, expertsIdxQualified),
  );
  const expertsDrewRound = useAppSelector((state) =>
    selectExpertsSome(state, expertsIdxDrewRound),
  );

  const handleRefresh = () => {
    if (drawLog != null) {
      dispatch(thunkFetchDrawLog(drawLog.index));
    }
  };

  const [onAddRound] = useAddRound();

  /**
   * 更新抽选结果
   * @param keys 新的要保留的专家
   */
  const handleRoundUpdate = async (keys: TExpert['index'][]) => {
    if (drawRound == null) {
      console.error('handle table update, drawRoundSecond or drawLog is null');
      return;
    }
    const roundNew = updateDrawRoundExperts(drawRound, keys);
    onUpdateRound(roundNew);
  };

  /**
   * 抽选按钮
   */
  const handleDraw = async () => {
    if (drawRound == null) {
      const expertsDrew = selectRandomExperts(expertsQualifiedInitial, drawCnt);
      const roundNew = createDrawRoundByExperts(
        expertsQualifiedInitial,
        expertsDrew,
      );
      await onAddRound(drawLog.index, roundNew);
      handleRefresh();
    } else {
      // 已经抽取过一次
      message.warning('已经抽取过一次，继续将覆盖当前结果！');
      const expertsDrew = selectRandomExperts(
        drawRound.current_experts,
        drawCnt,
        drawRound.status_list,
      );
      const roundNew = updateDrawRoundExperts(drawRound, expertsDrew);
      onUpdateRound(roundNew);
    }
  };

  const handleTableRemove = async (key: React.Key | React.Key[]) => {
    // 移除专家
    if (drawRound == null) {
      console.error('handle experts remove, drawRound or drawLog is null');
      return;
    }
    const keys = (Array.isArray(key) ? key : [key]).map(Number);
    const roundAfterRemove = removeDrawRoundExperts(drawRound, keys);
    const expertsDrewNew = selectRandomExperts(
      roundAfterRemove.current_experts,
      expertsIdxDrewRound.length,
      roundAfterRemove.status_list,
      false,
    );
    const roundNew = importDrawRoundExperts(roundAfterRemove, expertsDrewNew);
    console.log(
      'handle tab remove',
      keys,
      roundAfterRemove,
      expertsDrewNew,
      roundNew,
    );
    onUpdateRound(roundNew);
  };
  /**
   *
   * @param num 要抽取的专家数量 有可能是总数 也有可能是增加的数量
   * @param numIfAdd 是否是添加专家 默认是false 即专家总数
   */
  const handleImportExpertRandom: ISupplyDrawBtnProps['onDraw'] = async (
    num,
    numIfAdd,
  ) => {
    // 移除专家
    if (drawRound == null) {
      message.error('请先抽取一轮专家');
      console.error('handle experts add, drawRound or drawLog is null');
      return;
    }
    const numCal = numIfAdd ? num + expertsIdxDrewRound.length : num;
    if (numCal <= expertsIdxDrewRound.length) {
      message.error('递补专家数量不能小于已抽取专家数量');
      return;
    }
    const expertsDrewNew = selectRandomExperts(
      drawRound.current_experts,
      numCal,
      drawRound.status_list,
      false,
    );
    const roundNew = importDrawRoundExperts(drawRound, expertsDrewNew);
    onUpdateRound(roundNew);
  };
  const handleTableUpdate = () => {
    setExpertImportModalOpen(true);
  };

  const handleDrawLock: IDrawLockBtnProps['onChange'] = async (val) => {
    if (drawRound == null) {
      // 请先抽选
      message.error('请先抽选');
      return;
    }
    const cb = (ifCheck: boolean) => {
      console;
      if (!ifCheck) {
        return;
      }
      try {
        onUpdateRound({
          ...drawRound,
          if_log: val,
        });
      } catch (e) {
        console.error(e);
        message.error('更新锁定失败');
      }
    };
    if (val === 2) {
      // 解锁 需要密码验证
      dispatch(setCheckCB(cb));
      dispatch(setAdminCheckOpen(true));
    } else {
      cb(true); // 加锁，无需密码验证
    }
  };

  const tableTitle: IExpertTableProps['title'] = (data, selectedRowKeys) => (
    <ExpertTableTitleDrawRoundFirst
      onAdd={handleTableUpdate}
      onRemove={() => handleTableRemove(selectedRowKeys)}
      delBtnDisabled={selectedRowKeys.length <= 0}
      expertsCount={expertsIdxDrewRound.length}
    />
  );

  return (
    <div className={cn('vstack', 'gap-3', 'align-items-center')}>
      <div className={cn('hstack', 'gap-2')}>
        <DrawCntFrom disabled={true} value={drawCnt} />
        <DrawLockBtn
          onChange={handleDrawLock}
          lockBtnDisabled={ifLock === 1 || drawRound == null}
          unlockBtnDisabled={ifLock === 0 || ifLock === 2}
        />
      </div>
      <StartDrawBtn
        disabled={ifLock === 1}
        drawRound={drawRound ?? undefined}
        onDraw={handleDraw}
      />
      <SupplyDrawBtn
        disabled={ifLock === 1}
        onDraw={handleImportExpertRandom}
      />

      <div className={cn('vstack', 'gap-2')}>
        <ExpertTable
          experts={expertsDrewRound ?? []}
          title={tableTitle}
          statusList={expertsStatusDrewRound}
          getColumns={(data) =>
            getExpertTableColumnsDrawRoundFirst(
              data,
              getExpertTableActionColumnDrawRoundFirst(
                handleTableRemove,
                ifLock === 1,
              ),
            )
          }
        />
      </div>
      <ExpertImportModal
        open={expertImportModalOpen}
        setOpen={setExpertImportModalOpen}
        expertImported={expertsIdxDrewRound}
        expertsAll={expertsQualifiedRound ?? []}
        onModalOk={handleRoundUpdate}
      />
    </div>
  );
};
