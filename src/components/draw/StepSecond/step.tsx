import React, { FC } from 'react';
import { TDrawLog, IDrawRound } from '../../../api/handle';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import {
  selectDrawLog,
  selectDrawRoundFirst,
  thunkFetchDrawLog,
} from '../../../store/drawing';
import { selectExpertsIdx } from '../../../store/expert';
import { DrawStepFirstComp } from './StepComp';
import { useUpdateRound } from '../../../handle/round/hook';

export const DrawStepSecond: FC = () => {
  const dispatch = useAppDispatch();
  const expertsIdx = useAppSelector(selectExpertsIdx);

  const drawLogNow = useAppSelector(selectDrawLog);
  const drawRoundFirst = useAppSelector(selectDrawRoundFirst);

  const handleRefresh = () => {
    if (drawLogNow != null) {
      dispatch(thunkFetchDrawLog(drawLogNow.index));
    }
  };

  const [onUpdateRound] = useUpdateRound();

  const apiUpdateRoundLocal = async (drawLog: TDrawLog, roundNew: IDrawRound) =>
    await onUpdateRound(drawLog.index, 0, roundNew);

  if (drawLogNow == null) {
    return <span>选举记录为空 error</span>;
  }
  return (
    <DrawStepFirstComp
      drawLog={drawLogNow}
      drawRound={drawRoundFirst}
      expertsQualifiedInitial={expertsIdx ?? []}
      drawCnt={drawLogNow.first_cnt}
      onUpdateRound={async (round) => {
        await apiUpdateRoundLocal(drawLogNow, round);
        handleRefresh();
      }}
    />
  );
};
