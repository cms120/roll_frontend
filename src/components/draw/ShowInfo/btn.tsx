import React, { FC } from 'react';
import { Button } from 'antd';

export interface IDrawShowInfoBtnProps {
  onClick: (ifShow: boolean) => void;
  ifShow: boolean;
}
export const DrawShowInfoBtn: FC<IDrawShowInfoBtnProps> = ({
  onClick,
  ifShow = false,
}) => {
  return (
    <Button onClick={() => onClick(!ifShow)} type="primary">
      {ifShow ? '隐藏' : '显示'}抽选信息
    </Button>
  );
};
