import { Menu, MenuProps } from 'antd';
import React, { FC, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const items: MenuProps['items'] = [
  {
    label: '后台首页',
    key: '/admin',
  },
];

export const MenuSideAdmin: FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const onClick: MenuProps['onClick'] = ({ key }) => {
    navigate(key);
  };
  const [current, setCurrent] = React.useState('');
  useEffect(() => {
    if (location && location.pathname !== current) {
      setCurrent(location.pathname);
    }
  }, [location]);

  return (
    <Menu
      onClick={onClick}
      selectedKeys={[current]}
      mode="vertical"
      items={items}
    />
  );
};
