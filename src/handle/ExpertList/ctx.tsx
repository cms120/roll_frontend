import React, {
  createContext,
  Dispatch,
  FC,
  Reducer,
  useContext,
  useReducer,
} from 'react';
import { TExpert } from '../../api/handle';

type TState = {
  modalAddOpen: boolean;
  modalUpdateOpen: boolean;
  modalLoadOpen: boolean;

  rowEditing: TExpert | null;
  tableSearchWord: string;
};

const initialState: TState = {
  modalAddOpen: false,
  modalUpdateOpen: false,
  modalLoadOpen: false,

  rowEditing: null,
  tableSearchWord: '',
};

type TReducerAction =
  | {
      type: 'setModalAddOpen' | 'setModalUpdateOpen' | 'setModalLoadOpen';
      payload: boolean;
    }
  | {
      type: 'setRowEditing';
      row: TExpert;
    }
  | {
      type: 'setTableSearchWord';
      word: string;
    };

const ctx = createContext<TState>(initialState);

const dispatchCtx = createContext<Dispatch<TReducerAction>>(() =>
  console.error('No Expert List Dispatch Provider found'),
);
type TReducer = Reducer<TState, TReducerAction>;
const reducer: TReducer = (state, action) => {
  switch (action.type) {
    case 'setModalAddOpen': {
      return {
        ...state,
        modalAddOpen: action.payload,
      };
    }
    case 'setModalUpdateOpen': {
      return {
        ...state,
        modalUpdateOpen: action.payload,
      };
    }
    case 'setModalLoadOpen': {
      return {
        ...state,
        modalLoadOpen: action.payload,
      };
    }
    case 'setRowEditing': {
      return {
        ...state,
        rowEditing: action.row,
      };
    }
    case 'setTableSearchWord': {
      return {
        ...state,
        tableSearchWord: action.word,
      };
    }
    default: {
      throw Error('Unknown action: ' + action);
    }
  }
};
export const ExpertListProvider: FC<{
  children: React.ReactNode;
}> = ({ children }) => {
  const [value, dispatch] = useReducer(reducer, initialState);

  return (
    <ctx.Provider value={value}>
      <dispatchCtx.Provider value={dispatch}>{children}</dispatchCtx.Provider>
    </ctx.Provider>
  );
};

export const useExpertListCtx = () => useContext(ctx);
export const useExpertListDispatch = () => useContext(dispatchCtx);
