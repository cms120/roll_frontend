import { TDrawLog, TFetchStatus } from '../../api/handle';
import {
  useMessageWhenStatusChange,
  useRequestAsync,
} from '../../hooks/request';
import { apiAddSelectLog } from '../../api/draw';

export const useAddSelectLog = (): [
  (
    ...args: Parameters<typeof apiAddSelectLog>
  ) => Promise<Array<TDrawLog> | null>,
  TFetchStatus,
  Array<TDrawLog> | null,
] => {
  const [execute, status, data, error] = useRequestAsync(apiAddSelectLog);

  const msgError =
    '添加抽选失败' + (error?.msg ? `: ${error.msg}` : JSON.stringify(error));
  useMessageWhenStatusChange(status, '添加抽选成功', msgError);

  return [execute, status, data];
};
