import { useState } from 'react';
import { apiAddExpert, apiDelExpert, apiUpdateExpert } from '../../api/expert';
import { message } from 'antd';
import { TExpert, TFetchStatus } from '../../api/handle';
import {
  useMessageWhenStatusChange,
  useRequestAsync,
} from '../../hooks/request';

export const useAddExpert = (): [
  (...args: Parameters<typeof apiAddExpert>) => Promise<TExpert | null>,
  TFetchStatus,
  TExpert | null,
] => {
  const [execute, status, data, error] = useRequestAsync(apiAddExpert);

  const msgError =
    '添加专家失败' + (error?.msg ? `: ${error.msg}` : JSON.stringify(error));
  useMessageWhenStatusChange(status, '添加专家成功', msgError);

  return [execute, status, data];
};

export const useUpdateExpert = (): [
  (...args: Parameters<typeof apiUpdateExpert>) => Promise<TExpert | null>,
  TFetchStatus,
  TExpert | null,
] => {
  const [execute, status, data, error] = useRequestAsync(apiUpdateExpert);

  const msgError =
    '更新专家失败' + (error?.msg ? `: ${error.msg}` : JSON.stringify(error));
  useMessageWhenStatusChange(status, '更新专家成功', msgError);

  return [execute, status, data];
};

export const useDelExpert = (): [
  TFetchStatus,
  (key: TExpert['index'] | TExpert['index'][]) => Promise<void>,
] => {
  const [status, setStatus] = useState<TFetchStatus>('idle');
  const onDel = async (key: TExpert['index'] | TExpert['index'][]) => {
    try {
      let keys: TExpert['index'][] = [];
      if (Array.isArray(key)) {
        keys = key;
      } else {
        keys.push(key);
      }
      const res = await Promise.all(keys.map((k) => apiDelExpert(k)));

      if (res.length === 1) {
        message.success('删除成功');
      } else if (res.length > 1) {
        message.success('批量删除成功');
      }
    } catch (e) {
      console.error(e);
      message.error('删除失败');
    }
  };
  return [status, onDel];
};
