import { TDrawLog, IDrawRound, TFetchStatus } from '../../api/handle';
import {
  useMessageWhenStatusChange,
  useRequestAsync,
} from '../../hooks/request';
import { apiAddRound, apiUpdateRound } from '../../api/round';

export const useAddRound = (): [
  (...args: Parameters<typeof apiAddRound>) => Promise<IDrawRound | null>,
  TFetchStatus,
  IDrawRound | null,
] => {
  const [execute, status, data, error] = useRequestAsync(apiAddRound);

  const msgError =
    '抽选失败' + (error?.msg ? `: ${error.msg}` : JSON.stringify(error));
  useMessageWhenStatusChange(status, '抽选成功', msgError);

  return [execute, status, data];
};

export const useUpdateRound = (): [
  (...args: Parameters<typeof apiUpdateRound>) => Promise<TDrawLog | null>,
  TFetchStatus,
  TDrawLog | null,
] => {
  const [execute, status, data, error] = useRequestAsync(apiUpdateRound);

  const msgError =
    '更新选举轮失败' + (error?.msg ? `: ${error.msg}` : JSON.stringify(error));
  useMessageWhenStatusChange(status, '更新选举轮成功', msgError);

  return [execute, status, data];
};
