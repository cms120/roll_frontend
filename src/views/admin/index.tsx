import React, { FC } from 'react';
import { Outlet } from 'react-router-dom';
import { MenuSideAdmin } from '../../components/MenuSideAdmin';
import cn from 'classnames';

export const AdminPage: FC = () => {
  return (
    <div
      className={cn(
        'hstack',
        'gap-2',
        'p-2',
        'align-items-start',
        'w-100',
        'h-100',
      )}
    >
      <MenuSideAdmin />
      <Outlet />
    </div>
  );
};
