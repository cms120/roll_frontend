import React, { FC, useEffect, useState } from 'react';
import cn from 'classnames';
import {
  DrawStepFirst,
  DrawSteps,
  DrawStepsControl,
  DrawStepSecond,
  DrawStepThird,
} from '../../../components/draw';
import { useParams, useSearchParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import {
  selectDrawLog,
  setDrawLog,
  thunkFetchDrawLog,
} from '../../../store/drawing';
import { thunkFetchExperts } from '../../../store/expert';

export const DrawPage: FC = () => {
  const dispatch = useAppDispatch();

  const { drawId } = useParams();
  const [searchParams, setSearchParams] = useSearchParams();

  const drawLog = useAppSelector(selectDrawLog);
  useEffect(() => {
    dispatch(thunkFetchExperts());
  }, []);

  useEffect(() => {
    if (drawId == null) {
      dispatch(setDrawLog(null));
    }
    if (drawId != null) {
      dispatch(thunkFetchDrawLog(Number(drawId)));
    } else {
      dispatch(setDrawLog(null));
    }
  }, [drawId]);

  // get from searchParams
  const current = parseInt(searchParams.get('current') ?? '0');
  const setCurrent = (current: number) => {
    setSearchParams({
      ...searchParams,
      current: current.toString(),
    });
  };
  const handleFinish = () => {
    console.log('finish');
  };
  // the state index has succeed
  const [stepSuccess, setStepSuccess] = useState(-1);
  useEffect(() => {
    if (drawLog == null) {
      setStepSuccess(-1);
    } else {
      if (drawLog.rounds.length > 1) {
        // 第三步已做完 已进行第二轮筛选
        setStepSuccess(2);
      } else if (drawLog.rounds.length > 0) {
        //第二步已做完 已进行第一轮筛选
        setStepSuccess(1);
      } else {
        // 第一步已做完 已创建选举记录
        setStepSuccess(0);
      }
    }
  }, [drawLog]);

  const Step1Node: FC = () => <DrawStepFirst />;
  const Step2Node: FC = () =>
    drawLog != null ? <DrawStepSecond /> : <span>找不到该选举记录</span>;
  const Step3Node: FC = () =>
    drawLog != null ? <DrawStepThird /> : <span>找不到该选举记录</span>;
  const Step4Node: FC = () => <span>抽选完成</span>;
  return (
    <div className={cn('vstack', 'gap-3')}>
      <div
        className={cn(
          'hstack',
          'gap-3',
          'align-items-center',
          'justify-content-center',
        )}
      >
        <DrawStepsControl
          current={current}
          setCurrent={setCurrent}
          onFinish={handleFinish}
          stepSuccess={stepSuccess}
        />

        <DrawSteps className={''} current={current} />
      </div>

      {current === 0 ? (
        <Step1Node />
      ) : current === 1 ? (
        <Step2Node />
      ) : current === 2 ? (
        <Step3Node />
      ) : current === 3 ? (
        <Step4Node />
      ) : null}
    </div>
  );
};
