import React, { FC } from 'react';
import cn from 'classnames';
import { MenuSideUser } from '../../components/MenuSideUser';
import { Outlet } from 'react-router-dom';

export const UserPage: FC = () => {
  return (
    <div>
      <div
        className={cn(
          'hstack',
          'gap-2',
          'p-2',
          'align-items-start',
          'w-100',
          'h-100',
        )}
      >
        <MenuSideUser />
        <Outlet />
      </div>
    </div>
  );
};
