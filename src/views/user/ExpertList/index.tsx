import React, { FC, useEffect } from 'react';
import cn from 'classnames';
import { Button, message } from 'antd';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import {
  selectExperts,
  selectExpertsFetchStatus,
  thunkFetchExperts,
} from '../../../store/expert';
import {
  AddExpertModal,
  ExpertLoadModal,
  ExpertTable,
  ExpertTableTitleCommon,
  getExpertTableActionColumnCommon,
  getExpertTableColumnsCommon,
  IExpertTableProps,
  UpdateExpertModal,
} from '../../../components/expert';
import styles from './index.module.scss';
import { apiDelExpert } from '../../../api/expert';
import {
  ExpertListProvider,
  useExpertListDispatch,
} from '../../../handle/ExpertList/ctx';
import { isNil } from 'lodash';

export const ExpertList: FC = () => (
  <ExpertListProvider>
    <ExpertListContent />
  </ExpertListProvider>
);

const ExpertListContent: FC = () => {
  const experts = useAppSelector(selectExperts);
  const dispatch = useAppDispatch();
  const expertsFetchStatus = useAppSelector(selectExpertsFetchStatus);
  useEffect(() => {
    if (expertsFetchStatus === 'idle') {
      dispatch(thunkFetchExperts());
    }
  }, [expertsFetchStatus]);

  const getExpertList = () => dispatch(thunkFetchExperts());

  const expertsDispatch = useExpertListDispatch();

  const handleEditRow = (key: React.Key) => {
    const expertFind = experts?.find((expert) => expert.index === Number(key));
    if (isNil(expertFind)) {
      message.error('未找到专家');
      console.error('未找到专家', key, experts);
      return;
    }
    expertsDispatch({
      type: 'setRowEditing',
      row: expertFind,
    });
    expertsDispatch({
      type: 'setModalUpdateOpen',
      payload: true,
    });
  };

  const handleTableDel = async (key: React.Key | React.Key[]) => {
    try {
      let keys: React.Key[] = [];
      if (Array.isArray(key)) {
        keys = key;
      } else {
        keys.push(key);
      }
      const res = await Promise.all(keys.map((k) => apiDelExpert(Number(k))));

      if (res.length === 1) {
        message.success('删除成功');
      } else if (res.length > 1) {
        message.success('批量删除成功');
      }
    } catch (e) {
      console.error(e);
      message.error('删除失败');
    } finally {
      getExpertList();
    }
  };

  const tableTitle: IExpertTableProps['title'] = (data, selectedRowKeys) => (
    <ExpertTableTitleCommon
      onDel={() => handleTableDel(selectedRowKeys)}
      delBtnDisabled={selectedRowKeys.length <= 0}
    />
  );
  return (
    <div
      className={cn(
        styles['expert-list'],
        'vstack',
        'gap-2',
        'overflow-hidden',
      )}
    >
      <div className={cn('hstack', 'justify-content-between')}>
        <div className={cn('hstack', 'gap-1', 'justify-content-end')}>
          <Button
            type="primary"
            onClick={() =>
              expertsDispatch({
                type: 'setModalAddOpen',
                payload: true,
              })
            }
          >
            添加专家（表单）
          </Button>
          <Button
            type="primary"
            onClick={() =>
              expertsDispatch({
                type: 'setModalLoadOpen',
                payload: true,
              })
            }
          >
            导入专家（文件）
          </Button>
        </div>
      </div>
      <div
        className={cn(
          'vstack',
          'gap-1',
          'overflow-auto',
          'p-2',
          'expert-table-wrapper',
        )}
      >
        <ExpertTable
          experts={experts ?? []}
          loading={expertsFetchStatus === 'loading'}
          title={tableTitle}
          getColumns={(data) =>
            getExpertTableColumnsCommon(
              data,
              getExpertTableActionColumnCommon(handleEditRow, handleTableDel),
            )
          }
        />
      </div>

      <AddExpertModal onFinish={getExpertList} />
      <ExpertLoadModal onFinish={getExpertList} />
      <UpdateExpertModal onFinish={getExpertList} />
    </div>
  );
};
