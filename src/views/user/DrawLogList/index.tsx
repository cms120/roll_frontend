import React, { FC, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../../store/hook';
import cn from 'classnames';
import { DrawLogTable, IDrawLogTableProps } from '../../../components/DrawLog';
import styles from './index.module.scss';
import { useNavigate } from 'react-router-dom';
import { message } from 'antd';
import { UpdateDrawLogModal } from '../../../components/DrawLog/modify/ModalUpdate';
import { TDrawLog } from '../../../api/handle';
import { apiGetSelectLog } from '../../../api/draw';
import {
  selectDrawLogs,
  selectDrawLogsFetchStatus,
  thunkFetchDrawLogs,
} from '../../../store/drawing';

export const DrawLogList: FC = () => {
  const drawLogs = useAppSelector(selectDrawLogs);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const drawLogsFetchStatus = useAppSelector(selectDrawLogsFetchStatus);
  const tableLoading = drawLogsFetchStatus === 'loading';

  const [updateDrawLogModalOpen, setUpdateDrawLogModalOpen] = useState(false);
  const [drawLogEditing, setDrawLogEditing] = useState<TDrawLog | null>(null);
  useEffect(() => {
    getDrawLogs();
  }, []);

  const getDrawLogs = () => {
    dispatch(thunkFetchDrawLogs());
  };
  const handleContinueRow: IDrawLogTableProps['onContinueRow'] = (key) => {
    // search param: current
    const searchParams = new URLSearchParams();
    const drawLogFind = drawLogs.find(
      (drawLog) => drawLog.index === Number(key),
    );
    if (drawLogFind != null) {
      searchParams.set('current', String(drawLogFind.round_count));
    }
    navigate(`/draw/${key}/?${searchParams.toString()}`);
  };
  const handleEditRow: IDrawLogTableProps['onEditRow'] = async (key) => {
    try {
      const res = await apiGetSelectLog(Number(key));
      setDrawLogEditing(res.data);
      setUpdateDrawLogModalOpen(true);
    } catch (e) {
      console.error(e);
      message.error('获取抽选信息失败');
    }
  };
  return (
    <div
      className={cn(
        styles['draw-log-list'],
        'vstack',
        'gap-2',
        'overflow-hidden',
      )}
    >
      <div
        className={cn(
          'vstack',
          'gap-1',
          'overflow-auto',
          'p-2',
          'draw-log-table-wrapper',
        )}
      >
        <DrawLogTable
          data={drawLogs}
          loading={tableLoading}
          onContinueRow={handleContinueRow}
          onRefresh={getDrawLogs}
          onEditRow={handleEditRow}
        />
      </div>
      <UpdateDrawLogModal
        drawLogEditing={drawLogEditing}
        modalOpen={updateDrawLogModalOpen}
        setModalOpen={setUpdateDrawLogModalOpen}
        onFinish={getDrawLogs}
      />
    </div>
  );
};
