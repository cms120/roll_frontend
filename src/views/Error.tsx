import { useRouteError } from 'react-router-dom';
import React, { FC } from 'react';

export const ErrorPage: FC = () => {
  const error = useRouteError();
  console.error(error);

  return (
    <div id="error-page">
      <h1>Oops!</h1>
      <p>Sorry, an unexpected error has occurred.</p>
      <p>
        {/*// @ts-ignore*/}
        <i>{error.statusText || error.message}</i>
      </p>
    </div>
  );
};
