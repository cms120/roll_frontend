import { createHashRouter } from 'react-router-dom';
import React from 'react';
import { ExpertList } from '../views/user/ExpertList';
import { DrawPage } from '../views/user/DrawPage';
import { DrawList } from '../views/user/DrawList';
import { ErrorPage } from '../views/Error';
import { App } from '../App';
import { UserPage } from '../views/user';
import { AdminPage } from '../views/admin';
import { DrawLogList } from '../views/user/DrawLogList';

export const router = createHashRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: '/',
        element: <UserPage />,
        children: [
          {
            index: true,
            element: <ExpertList />,
          },
          {
            path: '/draw',
            element: <DrawPage />,
          },
          {
            path: '/draw/:drawId',
            element: <DrawPage />,
          },
          {
            path: '/drawList',
            element: <DrawLogList />,
          },
        ],
      },
      {
        path: 'admin',
        element: <AdminPage />,
        children: [
          {
            index: true,
            element: <DrawList />,
          },
        ],
      },
    ],
  },
]);
