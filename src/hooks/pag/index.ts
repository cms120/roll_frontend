import { PaginationProps } from 'antd';

export const useOnPagChange: PaginationProps['onChange'] = (
  pageNum,
  pageSize,
) => {};
