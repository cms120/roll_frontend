import { useEffect, useState } from 'react';
import { FormInstance, message } from 'antd';

import { TFetchStatus } from '../api/handle';

export function useValidateSubmitForm<T = never>(
  onSubmit: (formVal: T) => Promise<void>,
  formInstance: FormInstance<T> | null,
  onFinish?: () => void,
): [() => Promise<void>, TFetchStatus] {
  const [submitStatus, setSubmitStatus] = useState<TFetchStatus>('idle');
  const validateSubmit = async () => {
    if (submitStatus === 'loading') {
      return;
    }
    setSubmitStatus('loading');

    try {
      await formInstance?.validateFields();
    } catch (e) {
      setSubmitStatus('error');
      console.error(e);
      message.error('请检查表单');
      return;
    }
    const formVal = formInstance?.getFieldsValue();
    if (formVal == null) {
      setSubmitStatus('error');
      return;
    }
    try {
      await onSubmit(formVal);
      setSubmitStatus('success');
      onFinish && onFinish();
    } catch (e) {
      setSubmitStatus('error');
      console.error(e);
      message.error('表单提交失败');
    }
  };
  return [validateSubmit, submitStatus];
}

export function useWatchFormInitialValues<T = never>(
  formInstance: FormInstance<T>,
  initialValues?: T,
): void {
  useEffect(() => {
    formInstance.resetFields();
  }, [initialValues]);
}

export function useOnFormInstanceReady<T = never>(
  onFormInstanceReady: (form: FormInstance<T>) => void,
  form: FormInstance<T>,
) {
  useEffect(() => {
    onFormInstanceReady(form);
  }, [form]);
}
