import React from 'react';

export function useDelTableRows<T extends React.Key>(
  setSelectedRowKeys: React.Dispatch<React.SetStateAction<T[]>>,
  onDelete: (keys: T[]) => void,
) {
  const handleDel = (keys: T[]) => {
    onDelete(keys);
    setSelectedRowKeys([]);
  };
  return {
    handleDel,
  };
}
