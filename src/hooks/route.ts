import { useNavigate } from 'react-router-dom';
import { MenuProps } from 'antd';

export const useOnMenuItemClick = (): MenuProps['onClick'] => {
  const navigate = useNavigate();
  return ({ key }) => {
    navigate(key);
  };
};
