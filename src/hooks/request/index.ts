import { useCallback, useEffect, useState } from 'react';
import { message } from 'antd';
import { TBaseResponse, TFetchStatus } from '../../api/handle';

export function useAsync<R, P extends any[]>(
  asyncFunc: (...args: P) => Promise<R>,
): [
  (...args: P) => Promise<void>, // execute , hold the return value , make it a state
  TFetchStatus, // status, the status of fetch
  R | null, // the value of the return value
  any, // error
] {
  const [data, setData] = useState<R | null>(null);
  const [status, setStatus] = useState<TFetchStatus>('idle');
  const [error, setError] = useState<any>(null);
  const execute = useCallback(
    async (...args: P) => {
      setStatus('loading');
      setData(null);
      setError(null);
      try {
        const res = await asyncFunc(...args);
        // 请求成功，将数据写进 state，设置 loading 为 false
        setData(res);
        setStatus('success');
      } catch (err) {
        // 请求失败，设置 loading 为 false，并设置错误状态
        setError(err);
        setStatus('error');
      }
    },
    [asyncFunc],
  );

  return [execute, status, data, error];
}

export function useRequestAsync<R, P extends any[]>(
  asyncFunc: (...args: P) => Promise<TBaseResponse<R>>,
): [
  (...args: P) => Promise<R | null>, // execute , hold the return value , make it a state
  TFetchStatus, // status, the status of fetch
  R | null, // the value of the return value
  any, // error
] {
  const [data, setData] = useState<R | null>(null);
  const [status, setStatus] = useState<TFetchStatus>('idle');
  const [error, setError] = useState<any>(null);
  const execute = useCallback(
    async (...args: P) => {
      setStatus('loading');
      setData(null);
      setError(null);
      try {
        const res = await asyncFunc(...args);
        if (res.code === 200) {
          setData(res.data);
          setStatus('success');
          return res.data;
        } else {
          setError(res);
          setStatus('error');
        }
      } catch (err) {
        console.error(err);
        setError(err);
        setStatus('error');
      }
      return null;
    },
    [asyncFunc],
  );

  return [execute, status, data, error];
}

/**
 * message when status change
 */
export function useMessageWhenStatusChange(
  status: TFetchStatus,
  successMsg: string,
  errorMsg: string,
) {
  useEffect(() => {
    if (status === 'success' && successMsg) {
      message.success(successMsg);
    } else if (status === 'error' && errorMsg) {
      message.error(errorMsg);
    }
  }, [status]);
}
