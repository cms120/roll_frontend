import axios from 'axios';

// 响应拦截器
axios.interceptors.response.use(
  (response) => {
    if (response.data.status === 200) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
  // 服务器状态码不是200的情况
  (error) => {
    // NProgress.done();
    console.error(error);
    if (error.response && error.response.data instanceof Blob) {
      // 如果是文件操作的返回，由后续进行处理
      return Promise.resolve(error.response);
    }
    return Promise.reject(error);
  },
);
