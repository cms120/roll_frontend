import { axiosInstance } from './config';
import { TBaseResponse, TDrawLog, IDrawRound } from './handle';
import { AxiosResponse } from 'axios';

export const apiAddRound = async (
  select_log_index: number,
  round: IDrawRound,
) => {
  const params = {
    select_log_index,
  };
  const res = await axiosInstance.post<TBaseResponse<IDrawRound>>(
    '/round',
    round,
    {
      params,
    },
  );
  return res.data;
};

export const apiUpdateRound = async (
  select_log_index: number,
  round_index: number,
  data: IDrawRound,
) => {
  const params = {
    select_log_index,
    round_index,
  };
  const res = await axiosInstance.put<TBaseResponse<TDrawLog>>('/round', data, {
    params,
  });
  return res.data;
};

export const apiDelRound = async (
  select_log_index: number,
  round_index: number,
) => {
  return axiosInstance.delete<TBaseResponse>('/round', {
    params: {
      select_log_index,
      round_index,
    },
  });
};
