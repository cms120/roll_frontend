export type TExpert = {
  index: number;
  library: 'A' | 'B' | 'C';
  name: string;
  birth_date: string;
  organization: string;
  talent_title: string;
  inside_outside: '校内' | '校外';
  primary_discipline: string;
  secondary_discipline1: string;
  secondary_discipline2: string;
  phone_number: string;
  email: string;
};

export const ExpertNotFound = (): TExpert => ({
  index: Math.floor(Math.random() * 1000000) * -1, //随机生成一个负数 [0,100000)
  library: 'A',
  name: '未找到',
  birth_date: '2001-01-01T00:00:00',
  organization: '未找到',
  talent_title: '未找到',
  inside_outside: '校内',
  primary_discipline: '未找到',
  secondary_discipline1: '未找到',
  secondary_discipline2: '未找到',
  phone_number: '未找到',
  email: '未找到',
});

export const ExpertHide = (expert: TExpert): TExpert => ({
  index: expert.index,
  library: 'A',
  name: '隐藏',
  birth_date: '2001-01-01T00:00:00',
  organization: '隐藏',
  talent_title: '隐藏',
  inside_outside: '校内',
  primary_discipline: '隐藏',
  secondary_discipline1: '隐藏',
  secondary_discipline2: '隐藏',
  phone_number: '隐藏',
  email: '隐藏',
});

export type TDrawLogInList = Omit<TDrawLog, 'rounds'> & {
  round_count: number;
};

export type TDrawLog = {
  index: number;
  title: string;
  time: string;
  rounds: IDrawRound[];
  first_cnt: number; // 第一轮抽选人数
  second_cnt: number; // 第二轮抽选人数
};

/**
 * {
 *   "current_experts": [
 *     1,
 *     2
 *   ],
 *   "status_list": [
 *     0,
 *     1
 *   ]
 * }
 */

/*
status_list: [
0：没有抽中，
1：抽中了，但是被别人替换了，这个状态不能再参加抽签

2：没抽中，但是被替换上来了
3. 正常抽中，且没被替换
4：最后来的]
# 第一轮抽完后，status_list 只有0和3，然后通过替换操作，变成了1,2
# 第二轮从上一轮的2,3中开始抽取，变成了0,3
# 此时对所有的3开始发消息挨个确认来不来，如果确认不来，则会发生替换，他的状态变为1，替换者的状态变为2，并对替换者发消息确认来不来
# 所有能确认来的人的状态变为4，也是最后一轮结束的状态
*/
export type TDrawStatus = 0 | 1 | 2 | 3 | 4;

export interface IDrawRound {
  current_experts: number[];

  status_list: TDrawStatus[];

  if_log: 0 | 1 | 2; // 0 未锁定 1 锁定 2 锁定后又打开
}

export const DrawStatusText: Record<TDrawStatus, string> = {
  0: '未中',
  1: '中了，但被替换下去',
  2: '未中，但被替换上来',
  3: '正常抽中，且没被替换',
  4: '最后来的',
};
export type TBaseResponse<T = null> = {
  message?: string;
  code: number;
  data: T;
};

export type TFetchStatus = 'idle' | 'loading' | 'success' | 'error';
