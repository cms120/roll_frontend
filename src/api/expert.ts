import { axiosInstance } from './config';
import { TBaseResponse, TExpert } from './handle';

export const apiImportExperts = async (data: { excel_file: File }) => {
  return await axiosInstance.post<TBaseResponse>('/import', data, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
};

export const apiGetExpertList = async () => {
  const response =
    await axiosInstance.get<TBaseResponse<Array<TExpert>>>('/expertList');
  return response.data;
};

export const apiAddExpert = async (data: Omit<TExpert, 'index'>) => {
  // time process, 2024-03-31T16:00:00.000Z -> 1/2/10
  const date = new Date(data.birth_date);
  data.birth_date = `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`;

  const res = await axiosInstance.post<TBaseResponse>('/expert', {
    ...data,
    index: 0, // 后端会忽略这个字段
  });
  return res.data;
};

export const apiGetExpert = async (index: number) => {
  const res = await axiosInstance.get<TBaseResponse<TExpert>>('/expert', {
    params: {
      index: index,
    },
  });
  return res.data;
};
export const apiDelExpert = async (index: TExpert['index']) => {
  return await axiosInstance.delete<TBaseResponse>('/expert', {
    params: {
      index: index,
    },
  });
};
export const apiDelExpertList = async () => {
  return await axiosInstance.delete<TBaseResponse>('/expertList');
};
export const apiUpdateExpert = async (data: TExpert) => {
  const res = await axiosInstance.put<TBaseResponse>('/expert', data);
  return res.data;
};
