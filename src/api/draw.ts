import { TBaseResponse, TDrawLog, TDrawLogInList } from './handle';
import { axiosInstance } from './config';

export const apiAddSelectLog = async (
  params: Pick<TDrawLogInList, 'title' | 'time' | 'first_cnt' | 'second_cnt'>,
) => {
  const res = await axiosInstance.post<TBaseResponse<Array<TDrawLog>>>(
    '/selectLog',
    null,
    {
      params,
    },
  );
  return res.data;
};

export const apiGetSelectLogList = async () => {
  const res =
    await axiosInstance.get<TBaseResponse<Array<TDrawLogInList>>>(
      '/selectLogList',
    );
  return res.data;
};

export const apiGetSelectLog = async (index: number) => {
  const res = await axiosInstance.get<TBaseResponse<TDrawLog>>('/selectLog', {
    params: {
      index: index,
    },
  });
  return res.data;
};

export const apiDelSelectLog = async (index: number) => {
  return axiosInstance.delete<TBaseResponse>('/selectLog', {
    params: {
      index: index,
    },
  });
};

export const apiUpdateSelectLog = async (
  params: Pick<TDrawLog, 'index' | 'title' | 'time'>,
) => {
  return axiosInstance.put<TBaseResponse>('/selectLog', undefined, {
    params,
  });
};
