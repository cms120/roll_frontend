import React, { FC, useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import { message } from 'antd';
import { useAppSelector } from './store/hook';
import { selectArgsProps } from './store/message';
import cn from 'classnames';
import { MenuAppTop } from './components/MenuTop';
import { AdminCheck } from './components/admin/check';

export const App: FC = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const argsProps = useAppSelector(selectArgsProps);
  useEffect(() => {
    if (argsProps) {
      messageApi.open(argsProps);
    }
  }, [argsProps, messageApi]);
  return (
    <>
      <div id="message-holder">{contextHolder}</div>
      <AdminCheck />
      <div id="app" className={cn('vstack', 'gap-2', 'p-2', 'w-100', 'h-100')}>
        <MenuAppTop />
        <Outlet />
      </div>
    </>
  );
};
