# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

node version: lts/iron

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

### `npm run eject`

### electron build

```shell
pnpm install
pnpm run build # build react
pnpm run builder-win # build electron for windows
#pnpm run builder-mac # build electron for mac
```

https://cloud.tencent.com/developer/article/2071153

https://blog.csdn.net/raspi_fans/article/details/128683234
